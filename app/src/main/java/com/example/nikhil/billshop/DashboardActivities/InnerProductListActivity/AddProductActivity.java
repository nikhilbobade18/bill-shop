package com.example.nikhil.billshop.DashboardActivities.InnerProductListActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.Adapters.SpinnerAdapters.SpinnerCategoryAdapter;
import com.example.nikhil.billshop.Adapters.SpinnerAdapters.SpinnerUOMAdapter;
import com.example.nikhil.billshop.Models.ProductCategories;
import com.example.nikhil.billshop.Models.UOM;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class AddProductActivity extends AppCompatActivity {

    int companyid;
    int userid;

    int catid;
    String catname;
    int uomid;

    EditText et_prod_name, et_qty, et_uom, et_rate, et_UOM_IfOther, et_Category_IfOther;
    Button btn_add_product;
    Spinner spinner_Category, spinner_UOM;

    View btn_edit_uom, btn_edit_cat;

    SpinnerCategoryAdapter spinnerCategoryAdapter;
    SpinnerUOMAdapter spinnerUOMAdapter;
    ArrayList<ProductCategories> productCategories;
    ArrayList<ProductCategories> productCategoriesSpinner;
    ArrayList<UOM> uomCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        // productCategories = new ArrayList<>();
        // uomCategories = new ArrayList<>();

        findViewById(R.id.btnBackAddProduct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        userid = getIntent().getIntExtra("userId", 0);
        companyid = getIntent().getIntExtra("companyId", 0);

        hookUp();

        parseCategories();
        parseUOM();
        btnListener();
        btn_add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProduct();
            }
        });
    }

    void hookUp() {
        et_prod_name = findViewById(R.id.et_prod_name);
        et_qty = findViewById(R.id.et_qty);

        et_rate = findViewById(R.id.et_rate);
        et_UOM_IfOther = findViewById(R.id.et_UOM_IfOther);
        et_Category_IfOther = findViewById(R.id.et_Category_IfOther);
        spinner_Category = findViewById(R.id.spinner_Category);
        spinner_UOM = findViewById(R.id.spinner_UOM);

        btn_add_product = findViewById(R.id.btn_add_product);

        btn_edit_uom = findViewById(R.id.btn_edit_uom);
        btn_edit_cat = findViewById(R.id.btn_edit_cat);

    }


    void validateInputs() {
        if (et_prod_name.getText().toString().trim().equalsIgnoreCase("")) {
            et_prod_name.setError("Product name can not be blank");
            return;
        }
        if (et_qty.getText().toString().trim().equalsIgnoreCase("")) {
            et_qty.setError("quantity can not be blank");
            return;
        }

        if (et_rate.getText().toString().trim().equalsIgnoreCase("")) {
            et_rate.setError("rate can not be blank");
            return;
        }
        /*if (et_UOM_IfOther.getText().toString().trim().equalsIgnoreCase("")) {
            et_UOM_IfOther.setError("rate can not be blank");
            return;
        }*/
        if (et_Category_IfOther.getText().toString().trim().equalsIgnoreCase("")) {
            et_Category_IfOther.setError("Category can not be blank");
            return;
        }
    }

    private void addProduct() {


        try {

            validateInputs();

            String pname = et_prod_name.getText().toString().toUpperCase();
            String qty = et_qty.getText().toString().toUpperCase();
            String rate = et_rate.getText().toString().toUpperCase();


            String url = "http://160.153.234.207:8080/billshop/webapi/products/manage_products?action=" + 1 +
                    "&" + "pdtid=" + 0 +
                    "&" + "userid=" + userid +
                    "&" + "companyid=" + companyid +
                    "&" + "catid=" + catid +
                    "&" + "catname=" + catname +
                    "&" + "pdtname=" + pname +
                    "&" + "pdtqty=" + qty +
                    "&" + "puomtype=" + uomid +
                    "&" + "pdtrate=" + rate +
                    "&" + "logedinuserid=" + userid;

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(AddProductActivity.this, message, Toast.LENGTH_SHORT).show();

                                onBackPressed();
                            } else {
                                Toasty.success(AddProductActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("AddProdANerror", anError.getMessage());
                        }
                    });
        } catch (Exception ex) {
            Log.e("AddProduct", ex.getMessage());
        }
    }


    void btnListener() {
        spinner_Category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {

                try {
                    ProductCategories model2 = productCategories.get(pos);
                    // ProductCategories mSelected = (ProductCategories) parent.getItemAtPosition(pos);
                    Log.i("Id:", "" + model2.getCatid());
                    Log.i("NAme:", "" + model2.getCatname());

                    catid = model2.getCatid();
                    catname = model2.getCatname();
                } catch (Exception e) {
                    Log.e("ErrorSpCat:", "" + e);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
                Log.i("Message", "Nothing is selected");

            }


        });

        spinner_UOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    UOM model2 = uomCategories.get(i);
                    uomid = model2.getUomid();
                } catch (Exception e) {
                    Log.e("ErrorSpUom:", "" + e);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //region Add Category
        btn_edit_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(AddProductActivity.this);
                dialog.setContentView(R.layout.dialog_add_category);
                dialog.setCancelable(true);
                //dialog.setTitle("Title...");

                // set the custom dialog components - text, image and button
                final EditText et_cat_name = dialog.findViewById(R.id.et_cat_name);
                Button btn_insert_category = dialog.findViewById(R.id.btn_insert_category);

                // if button is clicked, close the custom dialog
                btn_insert_category.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (et_cat_name.getText().toString().trim().equalsIgnoreCase("")) {
                            et_cat_name.setError("Fill Category Name");
                            return;
                        }

                        String caturl = "http://160.153.234.207:8080/billshop/webapi/products/managecategory?action=" + 1 +
                                "&" + "catid=" + 0 +
                                "&" + "userid=" + userid +
                                "&" + "companyid=" + companyid +
                                "&" + "catname=" + et_cat_name.getText().toString().toUpperCase() +
                                "&" + "logedinuserid=" + userid;

                        Log.e("ChkUrl", caturl);


                        try {

                            AndroidNetworking.get(caturl)
                                    .setPriority(Priority.MEDIUM)
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            int lastinsertid = response.optInt("result");
                                            String message = response.optString("message");
                                            int haserror = response.optInt("haserror");

                                            if (haserror == 0) {
                                                Toasty.success(AddProductActivity.this, message, Toast.LENGTH_SHORT).show();
                                                parseCategories();
                                                dialog.dismiss();

                                            } else {
                                                Toasty.error(AddProductActivity.this, message, Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onError(ANError anError) {
                                            Log.e("AddProdANerror", anError.getMessage());
                                        }
                                    });
                        } catch (Exception ex) {
                            Log.e("AddProduct", ex.getMessage());
                        }


                    }
                });


                dialog.show();
            }
        });


        //endregion

        //region Add UOM
        btn_edit_uom.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {

                final Dialog dialog1 = new Dialog(AddProductActivity.this);
                dialog1.setContentView(R.layout.dialog_add_uom);
                dialog1.setCancelable(true);
                //dialog.setTitle("Title...");

                // set the custom dialog components - text, image and button
                final EditText et_cat_name = dialog1.findViewById(R.id.et_uom_name);
                Button btn_insert_category = dialog1.findViewById(R.id.btn_insert_uom);

                // if button is clicked, close the custom dialog
                btn_insert_category.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (et_cat_name.getText().toString().trim().equalsIgnoreCase("")) {
                            et_cat_name.setError("Fill UOM Name");
                            return;
                        }

                        String caturl = "http://160.153.234.207:8080/billshop/webapi/products/manageuom?action=" + 1 +
                                "&" + "uomid=" + 0 +
                                "&" + "userid=" + userid +
                                "&" + "companyid=" + companyid +
                                "&" + "uomname=" + et_cat_name.getText().toString().toUpperCase() +
                                "&" + "logedinuserid=" + userid;

                        Log.e("ChkUrl", caturl);


                        try {

                            AndroidNetworking.get(caturl)
                                    .setPriority(Priority.MEDIUM)
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            int lastinsertid = response.optInt("result");
                                            String message = response.optString("message");
                                            int haserror = response.optInt("haserror");

                                            if (haserror == 0) {
                                                Toasty.success(AddProductActivity.this, message, Toast.LENGTH_SHORT).show();
                                                parseUOM();
                                                dialog1.dismiss();

                                            } else {
                                                Toasty.error(AddProductActivity.this, message, Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onError(ANError anError) {
                                            Log.e("AddProdANerror", anError.getMessage());
                                        }
                                    });
                        } catch (Exception ex) {
                            Log.e("AddProduct", ex.getMessage());
                        }


                    }
                });


                dialog1.show();

            }
        });

        //endregion
    }

    private void parseCategories() {

        try {

            productCategories = new ArrayList<>();

            String url = "http://160.153.234.207:8080/billshop/webapi/products/categories?userid=" + userid +
                    "&" + "companyid=" + companyid;

            Log.e("ChkUrl", url);

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject member = response.getJSONObject(i);

                                        int catid = member.optInt("catid");
                                        String catname = member.getString("catname");


                                        productCategories.add(new ProductCategories(catid, catname));
                                        Log.e("chkProducts", "" + productCategories);

                                    } catch (Exception ex) {
                                        Log.e("parseCat: ", "" + ex);
                                    }
                                }
                                spinnerCategoryAdapter = new SpinnerCategoryAdapter(AddProductActivity.this, productCategories);
                                spinner_Category.setAdapter(spinnerCategoryAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("parseCat ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e("parseCat ", "" + ex);
        }
    }

    private void parseUOM() {

        try {

            uomCategories = new ArrayList<>();

            String url = "http://160.153.234.207:8080/billshop/webapi/products/uom?userid=" + userid +
                    "&" + "companyid=" + companyid;

            Log.e("ChkUrl", url);

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject member = response.getJSONObject(i);

                                        int catid = member.optInt("uomid");
                                        String catname = member.getString("uomname");


                                        uomCategories.add(new UOM(catid, catname));
                                        Log.e("chkProducts", "" + productCategories);

                                    } catch (Exception ex) {
                                        Log.e("parseCat: ", "" + ex);
                                    }
                                }
                                spinnerUOMAdapter = new SpinnerUOMAdapter(AddProductActivity.this, uomCategories);
                                spinner_UOM.setAdapter(spinnerUOMAdapter);


                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("parseCat ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e("parseCat ", "" + ex);
        }
    }

}
