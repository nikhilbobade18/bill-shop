package com.example.nikhil.billshop.DashboardActivities.InnerCustomerListActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.R;

import org.json.JSONObject;

import es.dmoral.toasty.Toasty;

public class AddCustomerActivity extends AppCompatActivity {

    int companyid;
    int userid;

    EditText et_first_name, et_last_name, et_mobile1, et_mobile2, et_email, et_address;
    Button btn_add_customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);

        userid = getIntent().getIntExtra("userId", 0);
        companyid = getIntent().getIntExtra("companyId", 0);


        hookUp();

        btn_add_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCustomer();
            }
        });

        findViewById(R.id.btnBackAddCustomer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void hookUp() {
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_mobile1 = findViewById(R.id.et_mobile1);
        et_mobile2 = findViewById(R.id.et_mobile2);
        et_email = findViewById(R.id.et_email);
        et_address = findViewById(R.id.et_address);
        btn_add_customer = findViewById(R.id.btn_add_customer);

    }

    void validateInputs() {
        if (et_first_name.getText().toString().trim().equalsIgnoreCase("")) {
            et_first_name.setError("Firstname can not be blank");
            return;
        }
        if (et_last_name.getText().toString().trim().equalsIgnoreCase("")) {
            et_last_name.setError("Last can not be blank");
            return;
        }
        if (et_mobile1.getText().toString().trim().equalsIgnoreCase("")) {
            et_mobile1.setError("Mobile Number can not be blank");
            return;
        }
        if (et_email.getText().toString().trim().equalsIgnoreCase("")) {
            et_email.setError("Email can not be blank");
            return;
        }
    }


    private void addCustomer() {


        try {

            validateInputs();


            String fname = et_first_name.getText().toString().toUpperCase();
            String lname = et_last_name.getText().toString().toUpperCase();
            String mobile1 = et_mobile1.getText().toString().toUpperCase();
            String mobile2 = et_mobile2.getText().toString().toUpperCase();
            String email = et_email.getText().toString().toUpperCase();
            String address = et_address.getText().toString().toUpperCase();

            String url = "http://160.153.234.207:8080/billshop/webapi/customer/manage?action=" + 1 +
                    "&" + "customerid=" + 0 +
                    "&" + "userid=" + userid +
                    "&" + "companyid=" + companyid +
                    "&" + "csttype=" + 1 +
                    "&" + "customerfname=" + fname +
                    "&" + "customerlname=" + lname +
                    "&" + "mobile1=" + mobile1 +
                    "&" + "mobile2=" + mobile2 +
                    "&" + "email=" + email +
                    "&" + "address=" + address +
                    "&" + "logedinuserid=" + 1;

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(AddCustomerActivity.this, message, Toast.LENGTH_SHORT).show();
                               /* startActivity(new Intent(AddCustomerActivity.this,CustomerActivity.class));
                                finish();*/
                                onBackPressed();
                            } else {
                                Toasty.success(AddCustomerActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ex) {
            Log.e("AddCustomer", ex.getMessage());
        }
    }

}
