package com.example.nikhil.billshop.Models;


public class Purchase {


    int companyid;
    String createdat;
    int cstid;
    double finalamt;
    int gst;
    double gstamt;
    int pdtid;
    int prcid;
    int prcqty;
    double prcrate;
    int userid;

    public Purchase() {
    }

    public Purchase(int companyid, String createdat, int cstid, double finalamt, int gst, double gstamt, int pdtid, int prcid, int prcqty, double prcrate, int userid) {
        this.companyid = companyid;
        this.createdat = createdat;
        this.cstid = cstid;
        this.finalamt = finalamt;
        this.gst = gst;
        this.gstamt = gstamt;
        this.pdtid = pdtid;
        this.prcid = prcid;
        this.prcqty = prcqty;
        this.prcrate = prcrate;
        this.userid = userid;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public int getCstid() {
        return cstid;
    }

    public void setCstid(int cstid) {
        this.cstid = cstid;
    }

    public double getFinalamt() {
        return finalamt;
    }

    public void setFinalamt(double finalamt) {
        this.finalamt = finalamt;
    }

    public int getGst() {
        return gst;
    }

    public void setGst(int gst) {
        this.gst = gst;
    }

    public double getGstamt() {
        return gstamt;
    }

    public void setGstamt(double gstamt) {
        this.gstamt = gstamt;
    }

    public int getPdtid() {
        return pdtid;
    }

    public void setPdtid(int pdtid) {
        this.pdtid = pdtid;
    }

    public int getPrcid() {
        return prcid;
    }

    public void setPrcid(int prcid) {
        this.prcid = prcid;
    }

    public int getPrcqty() {
        return prcqty;
    }

    public void setPrcqty(int prcqty) {
        this.prcqty = prcqty;
    }

    public double getPrcrate() {
        return prcrate;
    }

    public void setPrcrate(double prcrate) {
        this.prcrate = prcrate;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
}
