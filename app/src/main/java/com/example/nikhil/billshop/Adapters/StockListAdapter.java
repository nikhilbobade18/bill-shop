package com.example.nikhil.billshop.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.DashboardActivities.InnerProductListActivity.ProductInnerListActivity;
import com.example.nikhil.billshop.Models.Products;
import com.example.nikhil.billshop.R;

import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class StockListAdapter extends RecyclerView.Adapter<StockListAdapter.StockListViewHolder> {

    private Activity context;
    private ArrayList<Products> mProductsList;

    public StockListAdapter(Activity context, ArrayList<Products> mProductsList) {
        this.context = context;
        this.mProductsList = mProductsList;
    }

    @Override
    public StockListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_stocks_list, parent, false);
        return new StockListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(StockListViewHolder holder, final int position) {
        final Products item = mProductsList.get(position);

        final String tv_productName = item.getPdtname();

        final int tv_qty = item.getPdtqty();

        holder.tv_product_name_stock.setText(tv_productName);
        holder.tv_qty_stock.setText(String.valueOf(tv_qty));


        holder.mainLayoutMemberStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, ProductInnerListActivity.class);
                i.putExtra("productName", tv_productName);
                context.startActivity(i);

                Toast.makeText(context, "CLicked" + tv_productName, Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(v.getContext(), ViewMembersActivity.class);
                intent.putExtra("formId", item.getFormId());
                v.getContext().startActivity(intent);*/
            }
        });



    }

    private void parseJson(int productid, String pname, int catid, int companyid, int logedinuserid) {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/products/manage?action=" + 0 +
                    "&" + "pdtid=" + productid +
                    "&" + "userid=" + 1 +
                    "&" + "companyid=" + 1 +
                    "&" + "catid=" + 1 +
                    "&" + "catname=" + "delete" +
                    "&" + "pdtname=" + pname +
                    "&" + "pdtqty=" + 1 +
                    "&" + "puomtype=" + 1 +
                    "&" + "pdtrate=" + 1 +
                    "&" + "logedinuserid=" + logedinuserid;

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();

                                notifyDataSetChanged();

                            } else {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("parseJson", "" + anError.getErrorBody());
                            Log.e("parseJson", "" + anError.getErrorDetail());
                        }
                    });
        } catch (Exception ex) {
            Log.e("parseJson", "onCreate: ", ex);
        }
    }


    @Override
    public int getItemCount() {
        return mProductsList.size();
    }

    //region Search Filter (setFilter Code)
   /* public void setFilter(ArrayList<Member> newList) {
        mMemberList = new ArrayList<>();
        mMemberList.addAll(newList);
        notifyDataSetChanged();
    }*/

    public class StockListViewHolder extends RecyclerView.ViewHolder {


        LinearLayout mainLayoutMemberStock;
        /* RelativeLayout status_text_c;
         RelativeLayout status_text_s;*/
        TextView tv_product_name_stock;
        TextView tv_qty_stock;

        View btn_edit_product;
        View btn_delete_product;


        public StockListViewHolder(View itemView) {
            super(itemView);

            mainLayoutMemberStock = itemView.findViewById(R.id.mainLayoutMemberStock);
            /*status_text_c =itemView.findViewById(R.id.status_text_c);
            status_text_s =itemView.findViewById(R.id.status_text_s);*/
            tv_product_name_stock = itemView.findViewById(R.id.tv_product_name_stock);
            tv_qty_stock = itemView.findViewById(R.id.tv_qty_stock);



        }


    }
    //endregion


}
