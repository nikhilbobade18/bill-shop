package com.example.nikhil.billshop.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.HomeActivity;
import com.example.nikhil.billshop.Models.Sale;
import com.example.nikhil.billshop.R;

import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class SalesListAdapter extends RecyclerView.Adapter<SalesListAdapter.SalesListViewHolder> {

    private Activity context;
    private ArrayList<Sale> mSalesList;

    public SalesListAdapter(Activity context, ArrayList<Sale> mSalesList) {
        this.context = context;
        this.mSalesList = mSalesList;
    }

    @Override
    public SalesListAdapter.SalesListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_sale_list, parent, false);
        return new SalesListAdapter.SalesListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SalesListAdapter.SalesListViewHolder holder, final int position) {
        final Sale item = mSalesList.get(position);


        final int SaleGst = item.getGst();
        final double saleRate = item.getSalerate();
        final int SaleQty = item.getSaleqty();
        final double SaleGstAmt = item.getGstamt();
        final String SaleDate = item.getCreatedat();


        holder.tv_gstAmt_sale.setText(Double.toString(SaleGstAmt));
        holder.tv_gst_sale.setText(SaleGst);
        holder.tv_amt_sale.setText(Double.toString(saleRate));
        holder.tv_quantity_sale.setText(SaleQty);
        holder.tv_date_detail_sale.setText(SaleDate);


        holder.mainLayoutMemberSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, HomeActivity.class);
                //i.putExtra("companyDetails", item);
                context.startActivity(i);

            }
        });


    }


    private void parseJson(int companyid, String userid, int loggedinuser) {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/sales?userid=" + userid +
                    "&" + "companyid=" + companyid +
                    "&" + "logedinuserid=" + loggedinuser;

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                               /* startActivity(new Intent(AddCustomerActivity.this,CustomerActivity.class));
                                finish();*/
                                notifyDataSetChanged();

                            } else {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ex) {
            Log.e("parseJson", "onCreate: ", ex);
        }
    }

    @Override
    public int getItemCount() {
        return mSalesList.size();
    }

    //region Search Filter (setFilter Code)
    public void setFilter(ArrayList<Sale> newList) {
        mSalesList = new ArrayList<>();
        mSalesList.addAll(newList);
        notifyDataSetChanged();
    }

    public class SalesListViewHolder extends RecyclerView.ViewHolder {


        RelativeLayout mainLayoutMemberSale;

        TextView tv_clientname_sale, tv_product_sale, tv_amt_sale, tv_quantity_sale, tv_gst_sale, tv_gstAmt_sale, tv_date_detail_sale;
        //TextView txt_number;


        public SalesListViewHolder(View itemView) {
            super(itemView);

            mainLayoutMemberSale = itemView.findViewById(R.id.mainLayoutMemberSale);
            tv_clientname_sale = itemView.findViewById(R.id.tv_clientname_sale);
            tv_product_sale = itemView.findViewById(R.id.tv_product_sale);
            tv_amt_sale = itemView.findViewById(R.id.tv_amt_sale);
            tv_quantity_sale = itemView.findViewById(R.id.tv_quantity_sale);
            tv_gst_sale = itemView.findViewById(R.id.tv_gst_sale);
            tv_gstAmt_sale = itemView.findViewById(R.id.tv_gstAmt_sale);
            tv_date_detail_sale = itemView.findViewById(R.id.tv_date_detail_sale);


        }


    }
//endregion
}