package com.example.nikhil.billshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.Models.User;

import org.json.JSONObject;

import es.dmoral.toasty.Toasty;

public class CompanyRegistrationActivity extends AppCompatActivity {

    User user;

    EditText et_CompanyName, et_CompanyMobileNumber, et_CompanyEmail, et_Address;
    Button btn_CompanyRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_registration);

        user = getIntent().getParcelableExtra("userDetails");
        Log.e("userDetails", user.getUsername());
        Log.e("userDetails",""+ user.getUserid());


        hookUp();

        btn_CompanyRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                companyRegistration();
            }
        });

    }

    void hookUp() {
        et_CompanyName = findViewById(R.id.et_CompanyName);
        et_CompanyMobileNumber = findViewById(R.id.et_CompanyMobileNumber);
        et_CompanyEmail = findViewById(R.id.et_CompanyEmail);
        et_Address = findViewById(R.id.et_Address);
        btn_CompanyRegistration = findViewById(R.id.btn_CompanyRegistration);

    }

    void companyRegistration() {
        //region btnLogin
        if (et_CompanyName.getText().toString().trim().equalsIgnoreCase("")) {
            et_CompanyName.setError("Please Provide Username");
            return;
        }

        final String companyname = et_CompanyName.getText().toString().toUpperCase();
        final String mobileno = et_CompanyMobileNumber.getText().toString();
        final String email = et_CompanyEmail.getText().toString();
        final String address = et_Address.getText().toString();


        try {


            String url = "http://160.153.234.207:8080/billshop/webapi/companies/registration?action=" + 1 +
                    "&" + "companyid=" + 0 +
                    "&" + "userid=" + user.getUserid() +
                    "&" + "companyname=" + companyname +
                    "&" + "mobileno=" + mobileno +
                    "&" + "email=" + email +
                    "&" + "address=" + address +
                    "&" + "logedinuserid=" + user.getUserid();

            Log.e("ChkUrl", url);


            AndroidNetworking.post(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Log.e("ChkUrl", "" + response);
                            int result = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(CompanyRegistrationActivity.this, message, Toast.LENGTH_SHORT).show();
                                /*startActivity(new Intent(CompanyRegistrationActivity.this, CompanyActivity.class));
                                finish();*/
                                onBackPressed();

                            } else {
                                Toasty.error(CompanyRegistrationActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("AddCustomer", anError.getMessage());
                            Toasty.error(CompanyRegistrationActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception ex) {
            Log.e("AddCustomer", ex.getMessage());
        }

    }


}
