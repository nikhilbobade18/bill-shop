package com.example.nikhil.billshop.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable{

    int userid;
    String username;

    public User() {
    }

    public User(int userid, String username) {
        this.userid = userid;
        this.username = username;
    }

    protected User(Parcel in) {
        userid = in.readInt();
        username = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(userid);
        parcel.writeString(username);
    }
}
