package com.example.nikhil.billshop.Models;

public class ProductCategories {

    int catid;

    String catname;


    public ProductCategories() {
        super();
    }

    public ProductCategories(int catid, String catname) {
        super();
        this.catid = catid;

        this.catname = catname;
    }

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }


    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }


    @Override
    public String toString() {
        return "ProductCategories [catid=" + catid + ", catname="
                + catname + "]";
    }



}
