package com.example.nikhil.billshop.DashboardActivities.InnerProductListActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.nikhil.billshop.R;

public class ProductInnerListActivity extends AppCompatActivity {

    TextView tv_product_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_inner_list);

       /* findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

        String productName = getIntent().getStringExtra("productName");

        tv_product_name = findViewById(R.id.tv_product_name);
        tv_product_name.setText(productName);

        findViewById(R.id.iv_product_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductInnerListActivity.this, ProductTransactionAddActivity.class));
            }
        });
    }
}
