package com.example.nikhil.billshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.nikhil.billshop.Adapters.CompanyListAdapter;
import com.example.nikhil.billshop.Helper.ConnectionHelper;
import com.example.nikhil.billshop.Models.Company;
import com.example.nikhil.billshop.Models.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class CompanyActivity extends AppCompatActivity implements View.OnClickListener {

    User user;

    String TAG = "CompanyAct";
    RecyclerView rec_view_companies;
    CompanyListAdapter companyListAdapter;
    ArrayList<Company> companies;

    RelativeLayout rl_yes_internet, rl_no_internet;
    Button btnTryAgain;
    FloatingActionButton fab_add_company;

    TextView txtNoOfRecords;

    View btnBack_customer;


    public View viewSearch;
    public android.support.v7.widget.Toolbar toolbar;
    public RelativeLayout replaceToolbar, originalToolbar;
    public View viewReplaceBack, viewEventsReplaceBack, viewEventsReplaceClear, viewAdd, viewReplaceClear, viewAddEvent, viewSearchEvents;
    public EditText toolbarEditText, toolbarEventsEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);


        user = getIntent().getParcelableExtra("userDetails");
        Log.e("userDetails", user.getUsername());


        btnBack_customer = findViewById(R.id.btnBack_customer);

        btnBack_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        AndroidNetworking.initialize(this);
        companies = new ArrayList<>();

        hookUp();
        btnClickListener();

        rec_view_companies.setLayoutManager(new LinearLayoutManager(this));
        rec_view_companies.setItemAnimator(new DefaultItemAnimator());


        checkInternetConnection();

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternetConnection();
            }
        });


        viewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                originalToolbar.setVisibility(View.GONE);
                replaceToolbar.setVisibility(View.VISIBLE);
            }
        });


        toolbarEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // InputMethodManager imm = (InputMethodManager) this.getSystemService(Service.INPUT_METHOD_SERVICE);
                // imm.showSoftInput(toolbarEditText, 0);
                //imm.showSoftInputFromWindow(mainActivity.toolbarEditText.getWindowToken(), 0);
                filter(s.toString());
            }
        });


        viewReplaceBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarEditText.setText("");
                originalToolbar.setVisibility(View.VISIBLE);
                replaceToolbar.setVisibility(View.GONE);
            }
        });

        viewReplaceClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarEditText.setText("");
            }
        });

    }


    void hookUp() {
        rl_yes_internet = findViewById(R.id.rl_yes_internet);
        rl_no_internet = findViewById(R.id.rl_no_internet);
        rec_view_companies = findViewById(R.id.rec_view_companies);
        btnTryAgain = findViewById(R.id.btnTryAgain);
        fab_add_company = findViewById(R.id.fab_add_company);
        viewSearch = (View) findViewById(R.id.viewSearch);
        replaceToolbar = (RelativeLayout) findViewById(R.id.replaceToolbar);
        originalToolbar = (RelativeLayout) findViewById(R.id.originalToolbar);
        viewReplaceBack = (View) findViewById(R.id.viewReplaceBack);
        viewReplaceClear = (View) findViewById(R.id.viewReplaceClear);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        toolbarEditText = (EditText) findViewById(R.id.toolbarEditText);
        txtNoOfRecords = findViewById(R.id.txtNoOfRecords);

    }

    void btnClickListener() {
        fab_add_company.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add_company:

                Intent intent = new Intent(CompanyActivity.this, CompanyRegistrationActivity.class);
                intent.putExtra("userDetails", user);
                startActivity(intent);

                break;
        }
    }


    private void parseCompaniesJson() {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/companies?userid=" + user.getUserid();

            Log.e("ChkUrl", url);
            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {


                                        JSONObject member = response.getJSONObject(i);

                                        int userid = member.optInt("userid");
                                        int companyid = member.optInt("companyid");
                                        String companyname = member.optString("companyname");


                                        companies.add(new Company(userid, companyid, companyname));

                                        txtNoOfRecords.setText("Total:" + companies.size());
                                        Log.e("chkCustomers", "" + companies);

                                    } catch (Exception ex) {
                                        Log.e(TAG, "onResponse: ", ex);
                                    }
                                }
                                companyListAdapter = new CompanyListAdapter(CompanyActivity.this, companies);
                                rec_view_companies.setAdapter(companyListAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("onCreate: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e(TAG, "onCreate: ", ex);
        }
    }


    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<Company> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (Company member : companies) {
            String fname = member.getCompanyname().toLowerCase();

            if (fname.contains(text))
                filterdNames.add(member);
        }

        txtNoOfRecords.setText("Total:" + filterdNames.size());
        //calling a method of the adapter class and passing the filtered list
        companyListAdapter.setFilter(filterdNames);
    }


    //For Updating this while returning
    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    void checkInternetConnection() {
        ConnectionHelper connectionHelper = new ConnectionHelper(CompanyActivity.this);
        if (connectionHelper.isNetworkAvailable()) {
            rl_no_internet.setVisibility(View.GONE);
            rl_yes_internet.setVisibility(View.VISIBLE);
            parseCompaniesJson();
        } else {

            rl_no_internet.setVisibility(View.VISIBLE);
            rl_yes_internet.setVisibility(View.GONE);
            Toasty.error(CompanyActivity.this, "No Connection", Toast.LENGTH_SHORT).show();
        }


    }
}
