package com.example.nikhil.billshop.DashboardActivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.nikhil.billshop.Adapters.StockListAdapter;
import com.example.nikhil.billshop.Models.Globals;
import com.example.nikhil.billshop.Models.Products;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class StockActivity extends AppCompatActivity {

    RecyclerView recyclerViewStock;
    StockListAdapter stockListAdapter;
    ArrayList<Products> products;

    RelativeLayout rl_yes_internet, rl_no_internet;
    Button btnTryAgain;

    String TAG = "StockAct";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock);

        findViewById(R.id.btnBackStock).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        AndroidNetworking.initialize(this);
        products = new ArrayList<>();

        hookUp();

        recyclerViewStock.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewStock.setItemAnimator(new DefaultItemAnimator());


        parseProductsJson();

    }

    void hookUp() {
        recyclerViewStock = findViewById(R.id.recyclerViewStock);

    }


    private void parseProductsJson() {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/products?userid=" + Globals.userid +
                    "&" + "companyid=" + Globals.companyid;

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject member = response.getJSONObject(i);

                                        int companyid = member.optInt("companyid");
                                        int pdtid = member.optInt("pdtid");
                                        int pdtqty = member.optInt("pdtqty");
                                        double pdtrate = member.optDouble("pdtrate");
                                        int puomtype = member.optInt("puomtype");
                                        int userid = member.optInt("userid");
                                        int catid = member.optInt("categoryid");
                                        String catname = member.optString("categoryname");


                                        String pdtname = member.getString("pdtname");


                                        products.add(new Products(pdtid, userid, companyid, catid, catname, pdtname, pdtqty, puomtype, pdtrate));
                                        //Log.e("chkProducts", "" + products);

                                    } catch (Exception ex) {
                                        Log.e(TAG, "onResponse: ", ex);
                                    }
                                }
                                stockListAdapter = new StockListAdapter(StockActivity.this, products);
                                recyclerViewStock.setAdapter(stockListAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("onCreate: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e(TAG, "onCreate: ", ex);
        }
    }


}
