package com.example.nikhil.billshop.DashboardActivities.InnerCustomerListActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.Models.Customer;
import com.example.nikhil.billshop.R;

import org.json.JSONObject;

import es.dmoral.toasty.Toasty;

public class EditCustomerActivity extends AppCompatActivity {

    Customer customer;

    EditText et_first_name, et_last_name, et_mobile1, et_mobile2, et_email, et_address;
    Button btn_edit_customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_customer);

        findViewById(R.id.btnBackEditCustomer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        customer = getIntent().getParcelableExtra("customerDetails");
        Log.e("customerDetails", customer.getCustomerfname());

        hookUp();
        setToEditText();

        btn_edit_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editCustomer();
            }
        });

        //editCustomer();
    }




    void hookUp() {
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_mobile1 = findViewById(R.id.et_mobile1);
        et_mobile2 = findViewById(R.id.et_mobile2);
        et_email = findViewById(R.id.et_email);
        et_address = findViewById(R.id.et_address);
        btn_edit_customer = findViewById(R.id.btn_edit_customer);

    }

    void setToEditText() {
        et_first_name.setText(customer.getCustomerfname());
        et_last_name.setText(customer.getCustomerlname());
        et_mobile1.setText(customer.getMobile1());
        et_mobile2.setText(customer.getMobile2());
        et_email.setText(customer.getEmail());
        et_address.setText(customer.getAddress());
    }


    void validateInputs() {
        if (et_first_name.getText().toString().trim().equalsIgnoreCase("")) {
            et_first_name.setError("Firstname can not be blank");
            return;
        }
        if (et_last_name.getText().toString().trim().equalsIgnoreCase("")) {
            et_last_name.setError("Last can not be blank");
            return;
        }
        if (et_mobile1.getText().toString().trim().equalsIgnoreCase("")) {
            et_mobile1.setError("Mobile Number can not be blank");
            return;
        }
        if (et_email.getText().toString().trim().equalsIgnoreCase("")) {
            et_email.setError("Email can not be blank");
            return;
        }
    }

    private void editCustomer() {


        try {

            validateInputs();


            String fname = et_first_name.getText().toString();
            String lname = et_last_name.getText().toString();
            String mobile1 = et_mobile1.getText().toString();
            String mobile2 = et_mobile2.getText().toString();
            String email = et_email.getText().toString();
            String address = et_address.getText().toString();

            String url = "http://160.153.234.207:8080/billshop/webapi/customer/manage?action=" + 1 +
                    "&" + "customerid=" + customer.getCustomerid() +
                    "&" + "userid=" + customer.getUserid() +
                    "&" + "companyid=" + customer.getCompanyid() +
                    "&" + "csttype=" + customer.getCsttype() +
                    "&" + "customerfname=" + fname +
                    "&" + "customerlname=" + lname +
                    "&" + "mobile1=" + mobile1 +
                    "&" + "mobile2=" + mobile2 +
                    "&" + "email=" + email +
                    "&" + "address=" + address +
                    "&" + "logedinuserid=" + customer.getCustomerid();

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(EditCustomerActivity.this, message, Toast.LENGTH_SHORT).show();
                               /* startActivity(new Intent(AddCustomerActivity.this,CustomerActivity.class));
                                finish();*/
                                onBackPressed();
                            } else {
                                Toasty.success(EditCustomerActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ex) {
            Log.e("AddCustomer", ex.getMessage());
        }
    }

}
