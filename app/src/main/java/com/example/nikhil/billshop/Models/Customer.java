package com.example.nikhil.billshop.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Customer implements Parcelable {

    int customerid;
    int userid;
    int companyid;
    int  csttype;
    String customerfname;
    String customerlname;
    String mobile1;
    String mobile2;
    String email;
    String address;

    public Customer() {
    }

    public Customer(int customerid, int userid, int companyid, int csttype, String customerfname, String customerlname, String mobile1, String mobile2, String email, String address) {
        this.customerid = customerid;
        this.userid = userid;
        this.companyid = companyid;
        this.csttype = csttype;
        this.customerfname = customerfname;
        this.customerlname = customerlname;
        this.mobile1 = mobile1;
        this.mobile2 = mobile2;
        this.email = email;
        this.address = address;
    }

    protected Customer(Parcel in) {
        customerid = in.readInt();
        userid = in.readInt();
        companyid = in.readInt();
        csttype = in.readInt();
        customerfname = in.readString();
        customerlname = in.readString();
        mobile1 = in.readString();
        mobile2 = in.readString();
        email = in.readString();
        address = in.readString();
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public int getCustomerid() {
        return customerid;
    }

    public void setCustomerid(int customerid) {
        this.customerid = customerid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }

    public int getCsttype() {
        return csttype;
    }

    public void setCsttype(int csttype) {
        this.csttype = csttype;
    }

    public String getCustomerfname() {
        return customerfname;
    }

    public void setCustomerfname(String customerfname) {
        this.customerfname = customerfname;
    }

    public String getCustomerlname() {
        return customerlname;
    }

    public void setCustomerlname(String customerlname) {
        this.customerlname = customerlname;
    }

    public String getMobile1() {
        return mobile1;
    }

    public void setMobile1(String mobile1) {
        this.mobile1 = mobile1;
    }

    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(customerid);
        dest.writeInt(userid);
        dest.writeInt(companyid);
        dest.writeInt(csttype);
        dest.writeString(customerfname);
        dest.writeString(customerlname);
        dest.writeString(mobile1);
        dest.writeString(mobile2);
        dest.writeString(email);
        dest.writeString(address);
    }
}
