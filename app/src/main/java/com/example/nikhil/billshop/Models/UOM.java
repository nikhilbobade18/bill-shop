package com.example.nikhil.billshop.Models;

public class UOM {

    int uomid;

    String uomname;


    public UOM() {
        super();
    }

    public UOM(int uomid, String uomname) {
        this.uomid = uomid;
        this.uomname = uomname;
    }

    public int getUomid() {
        return uomid;
    }

    public void setUomid(int uomid) {
        this.uomid = uomid;
    }

    public String getUomname() {
        return uomname;
    }

    public void setUomname(String uomname) {
        this.uomname = uomname;
    }
}
