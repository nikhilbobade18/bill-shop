package com.example.nikhil.billshop.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.DashboardActivities.InnerCustomerListActivity.CustomerInnerListActivity;
import com.example.nikhil.billshop.HomeActivity;
import com.example.nikhil.billshop.Models.Company;
import com.example.nikhil.billshop.R;

import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class CompanyListAdapter extends RecyclerView.Adapter<CompanyListAdapter.CustomerListViewHolder> {

    private Activity context;
    private ArrayList<Company> mCompanyList;

    public CompanyListAdapter(Activity context, ArrayList<Company> mCompanyList) {
        this.context = context;
        this.mCompanyList = mCompanyList;
    }

    @Override
    public CompanyListAdapter.CustomerListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_company_list, parent, false);
        return new CompanyListAdapter.CustomerListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CompanyListAdapter.CustomerListViewHolder holder, final int position) {
        final Company item = mCompanyList.get(position);

        final String name = item.getCompanyname();

        final int id = item.getCompanyid();

        holder.txt_companyname.setText(name);



        holder.mainLayoutMemberCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, HomeActivity.class);
                i.putExtra("companyDetails", item);
                context.startActivity(i);

            }
        });


        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(context, EditCustomerActivity.class);
                intent.putExtra("customerDetails", item);
                context.startActivity(intent);*/
            }
        });


        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* parseJson(item.getCustomerid(), item.getCustomerfname(), item.getCustomerlname(), item.getCustomerid());
                notifyDataSetChanged();*/
            }
        });

    }


    private void parseJson(int customerid, String fname, String lname, int loggedinuser) {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/customer/manage?action=" + 0 +
                    "&" + "customerid=" + customerid +
                    "&" + "userid=" + 1 +
                    "&" + "companyid=" + 1 +
                    "&" + "csttype=" + 1 +
                    "&" + "customerfname=" + fname +
                    "&" + "customerlname=" + lname +
                    "&" + "mobile1=" + "delete" +
                    "&" + "mobile2=" + "delete" +
                    "&" + "email=" + "delete" +
                    "&" + "address=" + "delete" +
                    "&" + "logedinuserid=" + loggedinuser;

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                               /* startActivity(new Intent(AddCustomerActivity.this,CustomerActivity.class));
                                finish();*/
                                notifyDataSetChanged();

                            } else {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ex) {
            Log.e("parseJson", "onCreate: ", ex);
        }
    }

    @Override
    public int getItemCount() {
        return mCompanyList.size();
    }

    //region Search Filter (setFilter Code)
    public void setFilter(ArrayList<Company> newList) {
        mCompanyList = new ArrayList<>();
        mCompanyList.addAll(newList);
        notifyDataSetChanged();
    }

    public class CustomerListViewHolder extends RecyclerView.ViewHolder {


        RelativeLayout mainLayoutMemberCustomer;
        RelativeLayout status_text_c;
        RelativeLayout status_text_s;
        TextView txt_companyname;
        //TextView txt_number;
        View btn_edit;
        View btn_delete;


        public CustomerListViewHolder(View itemView) {
            super(itemView);

            mainLayoutMemberCustomer = itemView.findViewById(R.id.mainLayoutMemberCustomer);
            status_text_c = itemView.findViewById(R.id.status_text_c);
            status_text_s = itemView.findViewById(R.id.status_text_s);
            txt_companyname = itemView.findViewById(R.id.txt_companyname);
            btn_edit = itemView.findViewById(R.id.btn_edit);
            btn_delete = itemView.findViewById(R.id.btn_delete);


        }


    }
//endregion
}