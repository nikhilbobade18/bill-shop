package com.example.nikhil.billshop.DashboardActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.nikhil.billshop.Adapters.SalesListAdapter;
import com.example.nikhil.billshop.DashboardActivities.InnerSaleActivity.AddSaleActivity;
import com.example.nikhil.billshop.Models.Sale;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SaleActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "SaleAct";
    RecyclerView rec_view_sale;
    SalesListAdapter salesListAdapter;
    ArrayList<Sale> sales;

    int companyid;
    int userid;


    FloatingActionButton fab_add_sale;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);

        userid = getIntent().getIntExtra("userId", 0);
        companyid = getIntent().getIntExtra("companyId", 0);

        AndroidNetworking.initialize(this);
        sales = new ArrayList<>();

        hookUp();
        btnClickListener();

        rec_view_sale.setLayoutManager(new LinearLayoutManager(this));
        rec_view_sale.setItemAnimator(new DefaultItemAnimator());


        findViewById(R.id.btnBack_sale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        hookUp();
        parseSalesJson();


    }


    private void parseSalesJson() {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/sales?\n" +
                    "userid=" + userid + "&" + "companyid=" + companyid;

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {

                                        JSONObject member = response.getJSONObject(i);

                                        int companyid = member.optInt("companyid");
                                        String createdat = member.optString("createdat");
                                        int cstid = member.optInt("cstid");
                                        double finalamt = member.optDouble("finalamt");

                                        int gst = member.optInt("gst");
                                        double gstamt = member.optDouble("gstamt");
                                        int pdtid = member.optInt("pdtid");
                                        int saleid = member.optInt("saleid");
                                        int saleqty = member.optInt("saleqty");
                                        double salerate = member.optDouble("salerate");
                                        String companyname = member.optString("companyname");
                                        int userid = member.optInt("userid");

                                        sales.add(new Sale(companyid, createdat, cstid, finalamt, gst, gstamt, pdtid, saleid, saleqty, salerate, companyname, userid));

                                        Log.e("AutoSale", "" + sales);

                                    } catch (Exception ex) {
                                        Log.e("AutoSale", "onResponse: ", ex);
                                    }
                                }
                                salesListAdapter = new SalesListAdapter(SaleActivity.this, sales);
                                rec_view_sale.setAdapter(salesListAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("AutoSale: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e("AutoSale", "onCreate: ", ex);
        }
    }


    void hookUp() {
        fab_add_sale = findViewById(R.id.fab_add_sale);
        rec_view_sale = findViewById(R.id.rec_view_sale);
    }

    void btnClickListener() {
        fab_add_sale.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add_sale:

                Intent intent = new Intent(SaleActivity.this, AddSaleActivity.class);
                /*intent.putExtra("userId", userid);
                intent.putExtra("companyId", companyid);*/
                startActivity(intent);
                break;
        }
    }
}
