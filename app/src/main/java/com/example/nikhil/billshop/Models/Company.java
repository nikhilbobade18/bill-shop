package com.example.nikhil.billshop.Models;


import android.os.Parcel;
import android.os.Parcelable;

public class Company implements Parcelable {


    int userid;
    int companyid;
    String companyname;

    public Company() {
        super();
    }

    public Company(int userid, int companyid, String companyname) {
        super();
        this.userid = userid;
        this.companyid = companyid;
        this.companyname = companyname;
    }

    protected Company(Parcel in) {
        userid = in.readInt();
        companyid = in.readInt();
        companyname = in.readString();
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    @Override
    public String toString() {
        return "Company [userid=" + userid + ", companyid=" + companyid + ", companyname=" + companyname + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(userid);
        parcel.writeInt(companyid);
        parcel.writeString(companyname);
    }
}
