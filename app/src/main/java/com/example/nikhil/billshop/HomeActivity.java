package com.example.nikhil.billshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.DashboardActivities.CustomerActivity;
import com.example.nikhil.billshop.DashboardActivities.ProductActivity;
import com.example.nikhil.billshop.DashboardActivities.PurchaseActivity;
import com.example.nikhil.billshop.DashboardActivities.ReportsActivity;
import com.example.nikhil.billshop.DashboardActivities.SaleActivity;
import com.example.nikhil.billshop.DashboardActivities.StockActivity;
import com.example.nikhil.billshop.Models.Company;
import com.example.nikhil.billshop.Models.Counts;
import com.example.nikhil.billshop.Models.Globals;

import org.json.JSONObject;

import es.dmoral.toasty.Toasty;


public class HomeActivity extends AppCompatActivity {

    String TAG = "HomeAct";

    Company company;

    Button btnDrawer;

    int companyid;
    int userid;


    TextView tv_cust_count, tv_product_count, tv_sale_count, tv_purchase_count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        company = getIntent().getParcelableExtra("companyDetails");
        Log.e("userDetails", company.getCompanyname());
        userid = company.getUserid();
        companyid = company.getCompanyid();

        Globals globals = new Globals();
        globals.setUserid(userid);
        globals.setCompanyid(companyid);


        findViewById(R.id.imgV_customers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CustomerActivity.class);
                intent.putExtra("userId", userid);
                intent.putExtra("companyId", companyid);
                startActivity(intent);

            }
        });

        findViewById(R.id.imgV_stock).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toasty.info(HomeActivity.this,"Coming soon",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(HomeActivity.this, StockActivity.class));
            }
        });

        findViewById(R.id.imgV_purchase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, PurchaseActivity.class));
            }
        });

        findViewById(R.id.imgV_sale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, SaleActivity.class));
            }
        });

        findViewById(R.id.imgV_products).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ProductActivity.class);
                intent.putExtra("userId", userid);
                intent.putExtra("companyId", companyid);
                startActivity(intent);
            }
        });

        findViewById(R.id.imgV_reports).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, ReportsActivity.class));
                Toasty.info(HomeActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();
            }
        });


        tv_cust_count = findViewById(R.id.tv_cust_count);
        tv_product_count = findViewById(R.id.tv_product_count);
        tv_sale_count = findViewById(R.id.tv_sale_count);
        tv_purchase_count = findViewById(R.id.tv_purchase_count);

        showCounts();

        //showProductCount();
    }

    private void showCounts() {

        try {
            String url = "http://160.153.234.207:8080/billshop/webapi/counts" + "?userid=" + userid + "&" + "companyid=" + companyid;

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Counts counts = new Counts();
                            int customer = response.optInt("count_customers");
                            int products = response.optInt("count_products");
                            int sales = response.optInt("count_sales");
                            int purchases = response.optInt("count_purchases");

                            tv_cust_count.setText(String.valueOf(customer));
                            tv_product_count.setText(String.valueOf(products));
                            tv_sale_count.setText(String.valueOf(sales));
                            tv_purchase_count.setText(String.valueOf(purchases));

                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.e("chkCounts", anError.getErrorDetail());
                        }
                    });


        } catch (Exception ex) {
            Log.e(TAG, "onCreate: ", ex);
        }
    }

    /*private void showProductCount() {

        try {
            String url = "http://160.153.234.207:8080/billshop/webapi/products/getCount" + "?userid=" + 1 + "&" + "companyid=" + 1;

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            String count = response;
                            tv_product_count.setText(count);
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });


        } catch (Exception ex) {
            Log.e(TAG, "onCreate: ", ex);
        }
    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.about_us) {
            startActivity(new Intent(HomeActivity.this, AboutUs.class));
        }
        return true;
    }


    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

}
