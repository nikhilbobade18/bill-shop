package com.example.nikhil.billshop.DashboardActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.nikhil.billshop.Adapters.PurchaseListAdapter;
import com.example.nikhil.billshop.DashboardActivities.InnerPurchaseActivity.AddPurchaseActivity;
import com.example.nikhil.billshop.Models.Purchase;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class PurchaseActivity extends AppCompatActivity implements View.OnClickListener {
    String TAG = "PurchaseAct";
    RecyclerView rec_view_purchase;
    PurchaseListAdapter purchaseListAdapter;
    ArrayList<Purchase> purchases;

    int companyid;
    int userid;


    FloatingActionButton fab_add_Purchase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);


        userid = getIntent().getIntExtra("userId", 0);
        companyid = getIntent().getIntExtra("companyId", 0);

        AndroidNetworking.initialize(this);
        purchases = new ArrayList<>();

        hookUp();
        btnClickListener();

        rec_view_purchase.setLayoutManager(new LinearLayoutManager(this));
        rec_view_purchase.setItemAnimator(new DefaultItemAnimator());


        findViewById(R.id.btnBackPurchase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        hookUp();
        parseSalesJson();
    }

    private void parseSalesJson() {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/purchase?\n" +
                    "userid=" + userid + "&" + "companyid=" + companyid;

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {

                                        JSONObject member = response.getJSONObject(i);

                                        int companyid = member.optInt("companyid");
                                        String createdat = member.optString("createdat");
                                        int cstid = member.optInt("cstid");
                                        double finalamt = member.optDouble("finalamt");

                                        int gst = member.optInt("gst");
                                        double gstamt = member.optDouble("gstamt");
                                        int pdtid = member.optInt("pdtid");
                                        int prcid = member.optInt("prcid");
                                        int prcqty = member.optInt("prcqty");
                                        double prcrate = member.optDouble("prcrate");
                                        int userid = member.optInt("userid");

                                        purchases.add(new Purchase(companyid, createdat, cstid, finalamt, gst, gstamt, pdtid, prcid, prcqty, prcrate, userid));

                                        Log.e("AutoSale", "" + purchases);

                                    } catch (Exception ex) {
                                        Log.e("AutoSale", "onResponse: ", ex);
                                    }
                                }
                                purchaseListAdapter = new PurchaseListAdapter(PurchaseActivity.this, purchases);
                                rec_view_purchase.setAdapter(purchaseListAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("AutoSale: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e("AutoSale", "onCreate: ", ex);
        }
    }


    void hookUp() {
        fab_add_Purchase = findViewById(R.id.fab_add_Purchase);
        rec_view_purchase = findViewById(R.id.rec_view_purchase);
    }

    void btnClickListener() {
        fab_add_Purchase.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add_Purchase:

                Intent intent = new Intent(PurchaseActivity.this, AddPurchaseActivity.class);
                /*intent.putExtra("userId", userid);
                intent.putExtra("companyId", companyid);*/
                startActivity(intent);
                break;
        }
    }
}
