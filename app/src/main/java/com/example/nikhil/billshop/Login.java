package com.example.nikhil.billshop;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.Models.User;

import org.json.JSONObject;

import es.dmoral.toasty.Toasty;

public class Login extends AppCompatActivity {

    EditText et_Username, et_Password;
    Button btn_Login;
    TextView tv_ForgotPass, tv_SignUp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AndroidNetworking.initialize(Login.this);


        et_Username = (EditText) findViewById(R.id.et_Username);
        et_Password = (EditText) findViewById(R.id.et_Password);


        btn_Login = findViewById(R.id.btn_Login);

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //region btnLogin
                if (et_Username.getText().toString().trim().equalsIgnoreCase("")) {
                    et_Username.setError("Please Provide Username");
                    return;
                }

                if (et_Password.getText().toString().trim().equalsIgnoreCase("")) {
                    et_Password.setError("Please Provide Password");
                    return;
                }

                final String username = et_Username.getText().toString().toUpperCase();
                String password = et_Password.getText().toString();

                final String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);


                try {


                    String url = "http://160.153.234.207:8080/billshop/webapi/user/authuser?userid=" + 1 +
                            "&" + "username=" + username +
                            "&" + "deviceid=" + deviceId +
                            "&" + "password=" + password;

                    Log.e("ChkUrl", url);


                    AndroidNetworking.get(url)
                            .setPriority(Priority.MEDIUM)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    Log.e("ChkUrl", "" + response);
                                    int result = response.optInt("result");
                                    String message = response.optString("message");
                                    int haserror = response.optInt("haserror");

                                    if (haserror == 0) {
                                        Toasty.success(Login.this, message, Toast.LENGTH_SHORT).show();

                                        User user = new User();
                                        user.setUserid(result);
                                        user.setUsername(username);

                                        Intent intent = new Intent(Login.this, CompanyActivity.class);
                                        intent.putExtra("userDetails", user);
                                        finish();
                                        startActivity(intent);


                                    } else {
                                        Toasty.error(Login.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                    Log.e("AddCustomer", anError.getMessage());
                                    Toasty.error(Login.this, "Server Error", Toast.LENGTH_SHORT).show();
                                }
                            });
                } catch (Exception ex) {
                    Log.e("AddCustomer", ex.getMessage());
                }

            }
        });

        //endregion
    }


}
