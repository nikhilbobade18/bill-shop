package com.example.nikhil.billshop.DashboardActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.nikhil.billshop.Adapters.ProductsListAdapter;
import com.example.nikhil.billshop.DashboardActivities.InnerProductListActivity.AddProductActivity;
import com.example.nikhil.billshop.Helper.ConnectionHelper;
import com.example.nikhil.billshop.Models.Products;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class ProductActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "ProductAct";

    int companyid;
    int userid;

    RecyclerView rec_view_products;
    ProductsListAdapter productsListAdapter;
    ArrayList<Products> products;

    RelativeLayout rl_yes_internet, rl_no_internet;
    Button btnTryAgain;

    FloatingActionButton fab_add_product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);


        findViewById(R.id.btnBackProduct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        userid = getIntent().getIntExtra("userId", 0);
        companyid = getIntent().getIntExtra("companyId", 0);


        AndroidNetworking.initialize(this);
        products = new ArrayList<>();

        hookUp();

        btnClickListener();

        rec_view_products.setLayoutManager(new LinearLayoutManager(this));
        rec_view_products.setItemAnimator(new DefaultItemAnimator());


        checkInternetConnection();

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternetConnection();
            }
        });
    }

    void hookUp() {
        rl_yes_internet = findViewById(R.id.rl_yes_internet);
        rl_no_internet = findViewById(R.id.rl_no_internet);
        rec_view_products = findViewById(R.id.rec_view_products);
        btnTryAgain = findViewById(R.id.btnTryAgain);
        fab_add_product = findViewById(R.id.fab_add_product);
    }


    void btnClickListener() {
        fab_add_product.setOnClickListener(this);
    }

    private void parseProductsJson() {

        try {


            String url = "http://160.153.234.207:8080/billshop/webapi/products?userid=" + userid +
                    "&" + "companyid=" + companyid;

            Log.e("ChkProdURL ", url);

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject member = response.getJSONObject(i);

                                        int companyid = member.optInt("companyid");
                                        int pdtid = member.optInt("pdtid");
                                        int pdtqty = member.optInt("pdtqty");
                                        double pdtrate = member.optDouble("pdtrate");
                                        int puomtype = member.optInt("puomtype");
                                        int userid = member.optInt("userid");
                                        int catid = member.optInt("categoryid");
                                        String catname = member.optString("categoryname");

                                        String pdtname = member.optString("pdtname");

                                        products.add(new Products(pdtid, userid, companyid, catid, catname, pdtname, pdtqty, puomtype, pdtrate));

                                        //Log.e("chkProducts", "" + products);

                                    } catch (Exception ex) {
                                        Log.e(TAG, "onResponse: ", ex);
                                    }
                                }
                                productsListAdapter = new ProductsListAdapter(ProductActivity.this, products);
                                rec_view_products.setAdapter(productsListAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("onCreate: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e(TAG, "onCreate: ", ex);
        }
    }


    void checkInternetConnection() {
        ConnectionHelper connectionHelper = new ConnectionHelper(ProductActivity.this);
        if (connectionHelper.isNetworkAvailable()) {
            rl_no_internet.setVisibility(View.GONE);
            rl_yes_internet.setVisibility(View.VISIBLE);
            parseProductsJson();
        } else {

            rl_no_internet.setVisibility(View.VISIBLE);
            rl_yes_internet.setVisibility(View.GONE);
            Toasty.error(ProductActivity.this, "No Connection", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add_product:
                Intent intent = new Intent(ProductActivity.this, AddProductActivity.class);
                intent.putExtra("userId", userid);
                intent.putExtra("companyId", companyid);
                startActivity(intent);
                break;
        }
    }

    //For Updating this while returning
    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}
