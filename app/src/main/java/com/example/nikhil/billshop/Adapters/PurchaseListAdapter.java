package com.example.nikhil.billshop.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.HomeActivity;
import com.example.nikhil.billshop.Models.Purchase;
import com.example.nikhil.billshop.R;

import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class PurchaseListAdapter extends RecyclerView.Adapter<PurchaseListAdapter.PurchaseListViewHolder> {
    private Activity context;
    private ArrayList<Purchase> mPurchaseList;

    public PurchaseListAdapter(Activity context, ArrayList<Purchase> mPurchaseList) {
        this.context = context;
        this.mPurchaseList = mPurchaseList;
    }

    @Override
    public PurchaseListAdapter.PurchaseListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_purchase_list, parent, false);
        return new PurchaseListAdapter.PurchaseListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PurchaseListAdapter.PurchaseListViewHolder holder, final int position) {
        final Purchase item = mPurchaseList.get(position);


        final int purchaseGst = item.getGst();
        final double purchaseRate = item.getPrcrate();
        final int purchaseQty = item.getPrcqty();
        final double purchaseGstAmt = item.getGstamt();
        final String purchaseDate = item.getCreatedat();


        holder.tv_amtgst_Purchasae.setText(Double.toString(purchaseGstAmt));
        holder.tv_gst_Purchasae.setText(purchaseGst);
        holder.tv_amt_Purchasae.setText(Double.toString(purchaseRate));
        holder.tv_quantity_Purchasae.setText(purchaseQty);
        holder.tv_date_Purchasae.setText(purchaseDate);

        holder.mainLayoutMemberPurchasae.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, HomeActivity.class);
                //i.putExtra("companyDetails", item);
                context.startActivity(i);

            }
        });


    }


    private void parseJson(int companyid, String userid, int loggedinuser) {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/purchase?userid=" + userid +
                    "&" + "companyid=" + companyid +
                    "&" + "logedinuserid=" + loggedinuser;

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                               /* startActivity(new Intent(AddCustomerActivity.this,CustomerActivity.class));
                                finish();*/
                                notifyDataSetChanged();

                            } else {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ex) {
            Log.e("parseJson", "onCreate: ", ex);
        }
    }

    @Override
    public int getItemCount() {
        return mPurchaseList.size();
    }

    //region Search Filter (setFilter Code)
    public void setFilter(ArrayList<Purchase> newList) {
        mPurchaseList = new ArrayList<>();
        mPurchaseList.addAll(newList);
        notifyDataSetChanged();
    }

    public class PurchaseListViewHolder extends RecyclerView.ViewHolder {


        RelativeLayout mainLayoutMemberPurchasae;

        TextView tv_clientname_Purchasae, tv_product_Purchasae, tv_amt_Purchasae, tv_quantity_Purchasae, tv_gst_Purchasae, tv_amtgst_Purchasae, tv_date_Purchasae;
        //TextView txt_number;


        public PurchaseListViewHolder(View itemView) {
            super(itemView);

            mainLayoutMemberPurchasae = itemView.findViewById(R.id.mainLayoutMemberPurchasae);
            tv_clientname_Purchasae = itemView.findViewById(R.id.tv_clientname_Purchasae);
            tv_product_Purchasae = itemView.findViewById(R.id.tv_product_Purchasae);
            tv_amt_Purchasae = itemView.findViewById(R.id.tv_amt_Purchasae);
            tv_quantity_Purchasae = itemView.findViewById(R.id.tv_quantity_Purchasae);
            tv_gst_Purchasae = itemView.findViewById(R.id.tv_gst_Purchasae);
            tv_amtgst_Purchasae = itemView.findViewById(R.id.tv_amtgst_Purchasae);
            tv_date_Purchasae = itemView.findViewById(R.id.tv_date_Purchasae);


        }


    }
//endregion
}