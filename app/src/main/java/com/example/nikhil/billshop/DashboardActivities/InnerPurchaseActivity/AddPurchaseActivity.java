package com.example.nikhil.billshop.DashboardActivities.InnerPurchaseActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.nikhil.billshop.Adapters.AutoCompleteAdapters.AutocompleteCustomerAdapter;
import com.example.nikhil.billshop.Adapters.AutoCompleteAdapters.AutocompleteProductAdapter;
import com.example.nikhil.billshop.Adapters.AutoCompleteAdapters.AutocompleteSupplierAdapter;
import com.example.nikhil.billshop.Helper.Constants;
import com.example.nikhil.billshop.Models.Customer;
import com.example.nikhil.billshop.Models.Products;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddPurchaseActivity extends AppCompatActivity {
    public int finalQtyValue = 0;
    public int finalRateValue = 0;
    AutoCompleteTextView act_select_supp_name;
    AutoCompleteTextView act_select_purchaseProduct;
    AutocompleteSupplierAdapter autocompleteSupplierAdapter;
    AutocompleteProductAdapter autocompleteProductAdapter;
    ArrayList<Customer> customers;
    ArrayList<Products> products;
    TextView tv_subtotal_amt_purchase, tv_total_amt_purchase, tv_gsttotal_amt_purchase;
    EditText et_select_purchaseQuantity, et_purchaseRate, et_purchase_gst;
    Button btn_add_purchase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_purchase);

        customers = new ArrayList<>();
        products = new ArrayList<>();


        findViewById(R.id.btnBackPurchase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        hookUp();
        validateInputs();
        parseCustomersJson();

        parseProductsJson();

        calcAmt();

        act_select_purchaseProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pdtqty = products.get(position).getPdtqty();
                double pdtrate = products.get(position).getPdtrate();

            }
        });
    }

    void validateInputs() {

        if (act_select_supp_name.getText().toString().trim().equalsIgnoreCase("")) {
            act_select_supp_name.setError("supplier name can not be blank");
            return;
        }
        if (act_select_purchaseProduct.getText().toString().trim().equalsIgnoreCase("")) {
            act_select_purchaseProduct.setError("product name can not be blank");
            return;
        }
        if (et_select_purchaseQuantity.getText().toString().trim().equalsIgnoreCase("")) {
            et_select_purchaseQuantity.setError("quantity can not be blank");
            return;
        }
        if (et_purchaseRate.getText().toString().trim().equalsIgnoreCase("")) {
            et_purchaseRate.setError("rate can not be blank");
            return;
        }
    }

    private void parseCustomersJson() {

        try {

            AndroidNetworking.get(Constants.customerApi)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {


                                        JSONObject member = response.getJSONObject(i);

                                        int customerid = member.optInt("customerid");
                                        int userid = member.optInt("userid");
                                        int companyid = member.optInt("companyid");
                                        int csttype = member.optInt("csttype");

                                        String customerfname = member.optString("customerfname");
                                        String customerlname = member.optString("customerlname");
                                        String mobile1 = member.optString("mobile1");
                                        String mobile2 = member.optString("mobile2");
                                        String email = member.optString("email");
                                        String address = member.optString("address");

                                        customers.add(new Customer(customerid, userid, companyid, csttype, customerfname, customerlname, mobile1, mobile2, email, address));

                                        Log.e("AutoCust", "" + customers);

                                    } catch (Exception ex) {
                                        Log.e("AutoCust", "onResponse: ", ex);
                                    }
                                }
                                autocompleteSupplierAdapter = new AutocompleteSupplierAdapter(AddPurchaseActivity.this, customers);
                                act_select_supp_name.setAdapter(autocompleteSupplierAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("AutoCust: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e("AutoCust", "onCreate: ", ex);
        }
    }

    private void parseProductsJson() {

        try {

            AndroidNetworking.get(Constants.productApi)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject member = response.getJSONObject(i);

                                        int pdtid = member.optInt("pdtid");
                                        int userid = member.optInt("userid");
                                        int companyid = member.optInt("companyid");
                                        int categoryid = member.optInt("categoryid");
                                        String categoryname = member.optString("categoryname");
                                        String pdtname = member.optString("pdtname");
                                        int pdtqty = member.optInt("pdtqty");
                                        int puomtype = member.optInt("puomtype");
                                        double pdtrate = member.optDouble("pdtrate");


                                        products.add(new Products(pdtid, userid, companyid, categoryid, categoryname, pdtname, pdtqty, puomtype, pdtrate));

                                        Log.e("AutoProd", "" + products);

                                    } catch (Exception ex) {
                                        Log.e("AutoProduct: ", "" + ex);
                                    }
                                }
                                autocompleteProductAdapter = new AutocompleteProductAdapter(AddPurchaseActivity.this, products);
                                act_select_purchaseProduct.setAdapter(autocompleteProductAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("onCreate: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e("autoComplete: ", "" + ex);
        }
    }

    void calcAmt() {


        try {
            String rate = et_purchaseRate.getText().toString();
            finalRateValue = Integer.parseInt(rate);
        } catch (NumberFormatException ex) { // handle your exception

        }


        et_select_purchaseQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    finalQtyValue = Integer.parseInt(et_select_purchaseQuantity.getText().toString());
                    finalRateValue = Integer.parseInt(et_purchaseRate.getText().toString());

                    int subtotal = finalQtyValue * finalRateValue;
                    tv_subtotal_amt_purchase.setText(String.valueOf(subtotal));
                } catch (NumberFormatException e) {
                    System.out.println("not a number");
                }

            }
        });

        et_purchaseRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    finalQtyValue = Integer.parseInt(et_select_purchaseQuantity.getText().toString());
                    finalRateValue = Integer.parseInt(et_purchaseRate.getText().toString());

                    int subtotal = finalQtyValue * finalRateValue;
                    tv_subtotal_amt_purchase.setText(String.valueOf(subtotal));
                    tv_total_amt_purchase.setText(String.valueOf(subtotal));
                } catch (NumberFormatException e) {
                    System.out.println("not a number");
                }
            }
        });


        et_purchase_gst.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int subtotal = 0;
                    subtotal = finalQtyValue * finalRateValue;
                    double gst = Double.parseDouble(et_purchase_gst.getText().toString());
                    double finalGstValue = ((subtotal * gst) / 100);

                    tv_gsttotal_amt_purchase.setText(String.valueOf(finalGstValue));

                    double finalTotal = subtotal + finalGstValue;

                    tv_total_amt_purchase.setText(String.valueOf(finalTotal));
                } catch (NumberFormatException ex) { // handle your exception

                }
            }
        });


    }

    void hookUp() {
        act_select_supp_name = findViewById(R.id.act_select_supp_name);
        act_select_purchaseProduct = findViewById(R.id.act_select_purchaseProduct);
        tv_subtotal_amt_purchase = findViewById(R.id.tv_subtotal_amt_purchase);
        tv_total_amt_purchase = findViewById(R.id.tv_total_amt_purchase);
        tv_gsttotal_amt_purchase = findViewById(R.id.tv_gsttotal_amt_purchase);
        et_select_purchaseQuantity = findViewById(R.id.et_select_purchaseQuantity);
        et_purchaseRate = findViewById(R.id.et_purchaseRate);
        et_purchase_gst = findViewById(R.id.et_purchase_gst);
        btn_add_purchase = findViewById(R.id.btn_add_purchase);
    }
}
