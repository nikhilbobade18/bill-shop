package com.example.nikhil.billshop.DashboardActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.nikhil.billshop.Adapters.CustomerListAdapter;
import com.example.nikhil.billshop.DashboardActivities.InnerCustomerListActivity.AddCustomerActivity;
import com.example.nikhil.billshop.Helper.ConnectionHelper;
import com.example.nikhil.billshop.Helper.Constants;
import com.example.nikhil.billshop.HomeActivity;
import com.example.nikhil.billshop.Models.Customer;
import com.example.nikhil.billshop.Models.Globals;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class CustomerActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "CustAct";

    int companyid;
    int userid;

    RecyclerView rec_view_customers;
    CustomerListAdapter customerListAdapter;
    ArrayList<Customer> customers;

    RelativeLayout rl_yes_internet, rl_no_internet;
    Button btnTryAgain;
    FloatingActionButton fab_add_customer;

    TextView txtNoOfRecords;

    View btnBack_customer;


    public View viewSearch;
    public android.support.v7.widget.Toolbar toolbar;
    public RelativeLayout replaceToolbar, originalToolbar;
    public View viewReplaceBack, viewEventsReplaceBack, viewEventsReplaceClear, viewAdd, viewReplaceClear, viewAddEvent, viewSearchEvents;
    public EditText toolbarEditText, toolbarEventsEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        btnBack_customer = findViewById(R.id.btnBack_customer);

        btnBack_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        userid = getIntent().getIntExtra("userId", 0);
        companyid = getIntent().getIntExtra("companyId", 0);


        AndroidNetworking.initialize(this);
        customers = new ArrayList<>();

        hookUp();
        btnClickListener();

        rec_view_customers.setLayoutManager(new LinearLayoutManager(this));
        rec_view_customers.setItemAnimator(new DefaultItemAnimator());


        checkInternetConnection();

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternetConnection();
            }
        });


        viewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                originalToolbar.setVisibility(View.GONE);
                replaceToolbar.setVisibility(View.VISIBLE);
            }
        });


        toolbarEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // InputMethodManager imm = (InputMethodManager) this.getSystemService(Service.INPUT_METHOD_SERVICE);
                // imm.showSoftInput(toolbarEditText, 0);
                //imm.showSoftInputFromWindow(mainActivity.toolbarEditText.getWindowToken(), 0);
                filter(s.toString());
            }
        });


        viewReplaceBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarEditText.setText("");
                originalToolbar.setVisibility(View.VISIBLE);
                replaceToolbar.setVisibility(View.GONE);
            }
        });

        viewReplaceClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarEditText.setText("");
            }
        });
    }

    void hookUp() {
        rl_yes_internet = findViewById(R.id.rl_yes_internet);
        rl_no_internet = findViewById(R.id.rl_no_internet);
        rec_view_customers = findViewById(R.id.rec_view_customers);
        btnTryAgain = findViewById(R.id.btnTryAgain);
        fab_add_customer = findViewById(R.id.fab_add_customer);
        viewSearch = (View) findViewById(R.id.viewSearch);
        replaceToolbar = (RelativeLayout) findViewById(R.id.replaceToolbar);
        originalToolbar = (RelativeLayout) findViewById(R.id.originalToolbar);
        viewReplaceBack = (View) findViewById(R.id.viewReplaceBack);
        viewReplaceClear = (View) findViewById(R.id.viewReplaceClear);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        toolbarEditText = (EditText) findViewById(R.id.toolbarEditText);
        txtNoOfRecords = findViewById(R.id.txtNoOfRecords);

    }

    void btnClickListener() {
        fab_add_customer.setOnClickListener(this);
    }


    private void parseCustomersJson() {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/customer?userid=" + userid +
                    "&" + "companyid=" + companyid;


            String testurl = "http://160.153.234.207:8080/billshop/webapi/customer?userid=" + Globals.userid +
                    "&" + "companyid=" + Globals.companyid;
            Log.e("CAtestURL",testurl);

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {


                                        JSONObject member = response.getJSONObject(i);

                                        int customerid = member.optInt("customerid");
                                        int userid = member.optInt("userid");
                                        int companyid = member.optInt("companyid");
                                        int csttype = member.optInt("csttype");

                                        String customerfname = member.optString("customerfname");
                                        String customerlname = member.optString("customerlname");
                                        String mobile1 = member.optString("mobile1");
                                        String mobile2 = member.optString("mobile2");
                                        String email = member.optString("email");
                                        String address = member.optString("address");

                                        customers.add(new Customer(customerid, userid, companyid, csttype, customerfname, customerlname, mobile1, mobile2, email, address));

                                        txtNoOfRecords.setText("Total:" + customers.size());
                                        Log.e("chkCustomers", "" + customers);

                                    } catch (Exception ex) {
                                        Log.e(TAG, "onResponse: ", ex);
                                    }
                                }
                                customerListAdapter = new CustomerListAdapter(CustomerActivity.this, customers);
                                rec_view_customers.setAdapter(customerListAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("onCreate: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e(TAG, "onCreate: ", ex);
        }
    }


    void checkInternetConnection() {
        ConnectionHelper connectionHelper = new ConnectionHelper(CustomerActivity.this);
        if (connectionHelper.isNetworkAvailable()) {
            rl_no_internet.setVisibility(View.GONE);
            rl_yes_internet.setVisibility(View.VISIBLE);
            parseCustomersJson();
        } else {

            rl_no_internet.setVisibility(View.VISIBLE);
            rl_yes_internet.setVisibility(View.GONE);
            Toasty.error(CustomerActivity.this, "No Connection", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_add_customer:

                Intent intent = new Intent(CustomerActivity.this, AddCustomerActivity.class);
                intent.putExtra("userId", userid);
                intent.putExtra("companyId", companyid);
                startActivity(intent);
                break;
        }
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<Customer> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (Customer member : customers) {
            String fname = member.getCustomerfname().toLowerCase();
            String number1 = member.getMobile1();
            String lname = member.getCustomerlname().toLowerCase();
            if (fname.contains(text) || number1.contains(text) || lname.contains(text))
                filterdNames.add(member);
        }

        txtNoOfRecords.setText("Total:" + filterdNames.size());
        //calling a method of the adapter class and passing the filtered list
        customerListAdapter.setFilter(filterdNames);
    }


    //For Updating this while returning
    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}
