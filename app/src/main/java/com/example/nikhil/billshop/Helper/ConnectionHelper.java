package com.example.nikhil.billshop.Helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by SHAREQUE on 23/02/2018.
 */

public class ConnectionHelper {
    private Context actvityContext;

    public ConnectionHelper(Context actvityContext) {
        this.actvityContext = actvityContext;
    }


    //Checking Ineternet Connection
    public boolean isNetworkAvailable() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) actvityContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null;
        } catch (Exception e) {
            return false;
        }
    }
}
