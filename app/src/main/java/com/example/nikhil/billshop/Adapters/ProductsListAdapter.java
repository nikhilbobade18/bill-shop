package com.example.nikhil.billshop.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.DashboardActivities.InnerProductListActivity.EditProductActivity;
import com.example.nikhil.billshop.DashboardActivities.InnerProductListActivity.ProductInnerListActivity;
import com.example.nikhil.billshop.Models.Globals;
import com.example.nikhil.billshop.Models.Products;
import com.example.nikhil.billshop.R;

import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ProductListViewHolder> {

    private Activity context;
    private ArrayList<Products> mProductsList;

    public ProductsListAdapter(Activity context, ArrayList<Products> mProductsList) {
        this.context = context;
        this.mProductsList = mProductsList;
    }

    @Override
    public ProductListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_products_list, parent, false);
        return new ProductListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductListViewHolder holder, final int position) {
        final Products item = mProductsList.get(position);

        final String tv_productName = item.getPdtname();
        final int tv_category = item.getCategoryid();
        final String categoryname = item.getCategoryname();
        final int tv_qty = item.getPdtqty();
        final double tv_rate = item.getPdtrate();

        /*final String number = item.getMobile1();
        final int type = item.getCsttype();*/


        holder.tv_productName.setText(tv_productName);
        holder.tv_qty.setText(String.valueOf(tv_qty));
        holder.tv_category.setText(categoryname);
        holder.tv_rate.setText(String.valueOf(tv_rate));


        holder.mainLayoutMemberProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, ProductInnerListActivity.class);
                i.putExtra("productName", tv_productName);
                context.startActivity(i);

                Toast.makeText(context, "CLicked" + tv_productName, Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(v.getContext(), ViewMembersActivity.class);
                intent.putExtra("formId", item.getFormId());
                v.getContext().startActivity(intent);*/
            }
        });


        holder.btn_edit_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, EditProductActivity.class);
                intent.putExtra("productDetails", item);
                context.startActivity(intent);
            }
        });


        holder.btn_delete_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                parseJson(item.getPdtid(), item.getPdtname(), item.getCategoryid(), item.getCompanyid(), item.getUserid());
                notifyDataSetChanged();
            }
        });

    }

    private void parseJson(int productid, String pname, int catid, int companyid, int logedinuserid) {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/products/manage?action=" + 0 +
                    "&" + "pdtid=" + productid +
                    "&" + "userid=" + Globals.userid +
                    "&" + "companyid=" + Globals.companyid +
                    "&" + "catid=" + 1 +
                    "&" + "catname=" + "delete" +
                    "&" + "pdtname=" + pname +
                    "&" + "pdtqty=" + 1 +
                    "&" + "puomtype=" + 1 +
                    "&" + "pdtrate=" + 1 +
                    "&" + "logedinuserid=" + logedinuserid;

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();

                                notifyDataSetChanged();

                            } else {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("parseJson", "" + anError.getErrorBody());
                            Log.e("parseJson", "" + anError.getErrorDetail());
                        }
                    });
        } catch (Exception ex) {
            Log.e("parseJson", "onCreate: ", ex);
        }
    }


    @Override
    public int getItemCount() {
        return mProductsList.size();
    }

    //region Search Filter (setFilter Code)
   /* public void setFilter(ArrayList<Member> newList) {
        mMemberList = new ArrayList<>();
        mMemberList.addAll(newList);
        notifyDataSetChanged();
    }*/

    public class ProductListViewHolder extends RecyclerView.ViewHolder {


        LinearLayout mainLayoutMemberProducts;
        TextView tv_productName;
        TextView tv_category;
        TextView tv_qty, tv_rate;

        View btn_edit_product;
        View btn_delete_product;


        public ProductListViewHolder(View itemView) {
            super(itemView);

            mainLayoutMemberProducts = itemView.findViewById(R.id.mainLayoutMemberProducts);
            tv_productName = itemView.findViewById(R.id.tv_productName);
            tv_category = itemView.findViewById(R.id.tv_category);
            tv_qty = itemView.findViewById(R.id.tv_qty);
            tv_rate = itemView.findViewById(R.id.tv_rate);
            btn_edit_product = itemView.findViewById(R.id.btn_edit_product);
            btn_delete_product = itemView.findViewById(R.id.btn_delete_product);


        }


    }
    //endregion


}
