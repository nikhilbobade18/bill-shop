package com.example.nikhil.billshop.DashboardActivities.InnerSaleActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.nikhil.billshop.Adapters.AutoCompleteAdapters.AutocompleteCustomerAdapter;
import com.example.nikhil.billshop.Adapters.AutoCompleteAdapters.AutocompleteProductAdapter;
import com.example.nikhil.billshop.Models.Customer;
import com.example.nikhil.billshop.Models.Globals;
import com.example.nikhil.billshop.Models.Products;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddSaleActivity extends AppCompatActivity {

    public int finalQtyValue = 0;
    public int finalRateValue = 0;
    AutoCompleteTextView act_select_cust_name;
    AutoCompleteTextView act_select_product_name;
    AutocompleteCustomerAdapter autocompleteCustomerAdapter;
    AutocompleteProductAdapter autocompleteProductAdapter;
    ArrayList<Customer> customers;
    ArrayList<Products> products;
    TextView tv_avail_qty, tv_ratePerQty, tv_subtotal_amt, tv_total_amt, tv_gsttotal_amt;
    EditText et_sale_gst, et_select_saleQuantity, et_saleRate;
    Button btn_add_sale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sale);

        customers = new ArrayList<>();
        products = new ArrayList<>();


        findViewById(R.id.btnBackSale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        hookUp();

        validateInputs();

        parseCustomersJson();

        parseProductsJson();

        calcAmt();


        act_select_product_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pdtqty = products.get(position).getPdtqty();
                double pdtrate = products.get(position).getPdtrate();
                tv_avail_qty.setText(String.valueOf(pdtqty));
                tv_ratePerQty.setText(String.valueOf(pdtrate));
            }
        });

    }

    void validateInputs() {
        if (act_select_cust_name.getText().toString().trim().equalsIgnoreCase("")) {
            act_select_cust_name.setError("customer name can not be blank");
            return;
        }
        if (act_select_product_name.getText().toString().trim().equalsIgnoreCase("")) {
            act_select_product_name.setError("product name can not be blank");
            return;
        }
        if (et_select_saleQuantity.getText().toString().trim().equalsIgnoreCase("")) {
            et_select_saleQuantity.setError("quantity can not be blank");
            return;
        }
        if (et_saleRate.getText().toString().trim().equalsIgnoreCase("")) {
            et_saleRate.setError("rate can not be blank");
            return;
        }
    }

    private void parseCustomersJson() {

        try {


            String url = "http://160.153.234.207:8080/billshop/webapi/customer?userid=" + Globals.userid +
                    "&" + "companyid=" + Globals.companyid;


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {


                                        JSONObject member = response.getJSONObject(i);

                                        int customerid = member.optInt("customerid");
                                        int userid = member.optInt("userid");
                                        int companyid = member.optInt("companyid");
                                        int csttype = member.optInt("csttype");

                                        String customerfname = member.optString("customerfname");
                                        String customerlname = member.optString("customerlname");
                                        String mobile1 = member.optString("mobile1");
                                        String mobile2 = member.optString("mobile2");
                                        String email = member.optString("email");
                                        String address = member.optString("address");

                                        customers.add(new Customer(customerid, userid, companyid, csttype, customerfname, customerlname, mobile1, mobile2, email, address));

                                        Log.e("AutoCust", "" + customers);

                                    } catch (Exception ex) {
                                        Log.e("AutoCust", "onResponse: ", ex);
                                    }
                                }
                                autocompleteCustomerAdapter = new AutocompleteCustomerAdapter(AddSaleActivity.this, customers);
                                act_select_cust_name.setAdapter(autocompleteCustomerAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("AutoCust: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e("AutoCust", "onCreate: ", ex);
        }
    }

    private void parseProductsJson() {

        try {


            String url = "http://160.153.234.207:8080/billshop/webapi/products?userid=" + Globals.userid +
                    "&" + "companyid=" + Globals.companyid;

            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    try {


                                        JSONObject member = response.getJSONObject(i);

                                        int pdtid = member.optInt("pdtid");
                                        int userid = member.optInt("userid");
                                        int companyid = member.optInt("companyid");
                                        int categoryid = member.optInt("categoryid");
                                        String categoryname = member.optString("categoryname");
                                        String pdtname = member.optString("pdtname");
                                        int pdtqty = member.optInt("pdtqty");
                                        int puomtype = member.optInt("puomtype");
                                        double pdtrate = member.optDouble("pdtrate");


                                        products.add(new Products(pdtid, userid, companyid, categoryid, categoryname, pdtname, pdtqty, puomtype, pdtrate));

                                        Log.e("AutoProd", "" + products);

                                    } catch (Exception ex) {
                                        Log.e("AutoProduct: ", "" + ex);
                                    }
                                }
                                autocompleteProductAdapter = new AutocompleteProductAdapter(AddSaleActivity.this, products);
                                act_select_product_name.setAdapter(autocompleteProductAdapter);

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.e("onCreate: ", error.getErrorDetail());


                        }
                    });
        } catch (Exception ex) {
            Log.e("autoComplete: ", "" + ex);
        }
    }


    void calcAmt() {


        try {
            String rate = et_saleRate.getText().toString();
            finalRateValue = Integer.parseInt(rate);
        } catch (NumberFormatException ex) {
            Log.e("calcAmt", ex.getMessage());
        } catch (Exception ex) {
            Log.e("calcAmt", ex.getMessage());
        }


        et_select_saleQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    finalQtyValue = Integer.parseInt(et_select_saleQuantity.getText().toString());
                    finalRateValue = Integer.parseInt(et_saleRate.getText().toString());

                    int subtotal = finalQtyValue * finalRateValue;
                    tv_subtotal_amt.setText(String.valueOf(subtotal));
                } catch (NumberFormatException e) {
                    Log.e("calcAmt", e.getMessage());
                } catch (Exception ex) {
                    Log.e("calcAmt", ex.getMessage());
                }

            }
        });

        et_saleRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    finalQtyValue = Integer.parseInt(et_select_saleQuantity.getText().toString());
                    finalRateValue = Integer.parseInt(et_saleRate.getText().toString());

                    int subtotal = finalQtyValue * finalRateValue;
                    tv_subtotal_amt.setText(String.valueOf(subtotal));
                    tv_total_amt.setText(String.valueOf(subtotal));
                } catch (NumberFormatException e) {
                    Log.e("salerate", e.getMessage());
                } catch (Exception ex) {
                    Log.e("salerate", ex.getMessage());
                }
            }
        });


        et_sale_gst.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int subtotal = 0;
                    subtotal = finalQtyValue * finalRateValue;
                    double gst = Double.parseDouble(et_sale_gst.getText().toString());
                    double finalGstValue = ((subtotal * gst) / 100);

                    tv_gsttotal_amt.setText(String.valueOf(finalGstValue));

                    double finalTotal = subtotal + finalGstValue;

                    tv_total_amt.setText(String.valueOf(finalTotal));
                } catch (NumberFormatException ex) { // handle your exception
                    Log.e("saleGst", ex.getMessage());
                } catch (Exception ex) {
                    Log.e("saleGst", ex.getMessage());
                }
            }
        });


    }

    void hookUp() {
        act_select_cust_name = findViewById(R.id.act_select_cust_name);
        act_select_product_name = findViewById(R.id.act_select_product_name);
        tv_avail_qty = findViewById(R.id.tv_avail_qty);
        tv_ratePerQty = findViewById(R.id.tv_ratePerQty);
        tv_subtotal_amt = findViewById(R.id.tv_subtotal_amt);
        et_select_saleQuantity = findViewById(R.id.et_select_saleQuantity);
        et_saleRate = findViewById(R.id.et_saleRate);
        tv_total_amt = findViewById(R.id.tv_total_amt);
        et_sale_gst = findViewById(R.id.et_sale_gst);
        tv_gsttotal_amt = findViewById(R.id.tv_gsttotal_amt);
    }

}
