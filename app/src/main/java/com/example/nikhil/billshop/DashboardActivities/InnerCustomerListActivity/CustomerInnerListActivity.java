package com.example.nikhil.billshop.DashboardActivities.InnerCustomerListActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.nikhil.billshop.R;

public class CustomerInnerListActivity extends AppCompatActivity {

    TextView tv_welcome_username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_inner_list);

        /*findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

        String customerName = "Welcome " + getIntent().getStringExtra("customerName");

        tv_welcome_username = findViewById(R.id.tv_welcome_username);
        tv_welcome_username.setText(customerName);

        findViewById(R.id.iv_cust_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerInnerListActivity.this, CustomerTransactionAddActivity.class));
            }
        });
    }
}
