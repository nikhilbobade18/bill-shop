package com.example.nikhil.billshop.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.DashboardActivities.CustomerActivity;
import com.example.nikhil.billshop.DashboardActivities.InnerCustomerListActivity.AddCustomerActivity;
import com.example.nikhil.billshop.DashboardActivities.InnerCustomerListActivity.CustomerInnerListActivity;
import com.example.nikhil.billshop.DashboardActivities.InnerCustomerListActivity.EditCustomerActivity;
import com.example.nikhil.billshop.Models.Customer;
import com.example.nikhil.billshop.Models.Globals;
import com.example.nikhil.billshop.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerListViewHolder> {

    private Activity context;
    private ArrayList<Customer> mCutomerList;

    public CustomerListAdapter(Activity context, ArrayList<Customer> mCutomerList) {
        this.context = context;
        this.mCutomerList = mCutomerList;
    }

    @Override
    public CustomerListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_customers_list, parent, false);
        return new CustomerListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomerListViewHolder holder, final int position) {
        final Customer item = mCutomerList.get(position);

        final String fname = item.getCustomerfname();
        final String lname = item.getCustomerlname();
        final String number = item.getMobile1();
        final int type = item.getCsttype();

        final String fullname = fname + lname;

        holder.txt_name.setText(fname + " " + lname);
        holder.txt_number.setText(number);

        if (type == 1) {
            holder.status_text_c.setVisibility(View.VISIBLE);
            holder.status_text_s.setVisibility(View.GONE);
        } else {
            holder.status_text_c.setVisibility(View.GONE);
            holder.status_text_s.setVisibility(View.VISIBLE);
        }

        holder.mainLayoutMemberCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, CustomerInnerListActivity.class);
                i.putExtra("customerName", fullname);
                context.startActivity(i);

                Toast.makeText(context, "CLicked" + fullname, Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(v.getContext(), ViewMembersActivity.class);
                intent.putExtra("formId", item.getFormId());
                v.getContext().startActivity(intent);*/
            }
        });


        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, EditCustomerActivity.class);
                intent.putExtra("customerDetails", item);
                context.startActivity(intent);
            }
        });


        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                parseJson(item.getCustomerid(), item.getCustomerfname(), item.getCustomerlname(), item.getCustomerid());
                notifyDataSetChanged();
            }
        });

    }


    private void parseJson(int customerid, String fname, String lname, int loggedinuser) {

        try {

            String url = "http://160.153.234.207:8080/billshop/webapi/customer/manage?action=" + 0 +
                    "&" + "customerid=" + customerid +
                    "&" + "userid=" + Globals.userid +
                    "&" + "companyid=" + Globals.companyid +
                    "&" + "csttype=" + 1 +
                    "&" + "customerfname=" + fname +
                    "&" + "customerlname=" + lname +
                    "&" + "mobile1=" + "delete" +
                    "&" + "mobile2=" + "delete" +
                    "&" + "email=" + "delete" +
                    "&" + "address=" + "delete" +
                    "&" + "logedinuserid=" + loggedinuser;

            Log.e("CLA:ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                               /* startActivity(new Intent(AddCustomerActivity.this,CustomerActivity.class));
                                finish();*/
                                notifyDataSetChanged();

                            } else {
                                Toasty.success(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ex) {
            Log.e("parseJson", "onCreate: ", ex);
        }
    }

    @Override
    public int getItemCount() {
        return mCutomerList.size();
    }

    //region Search Filter (setFilter Code)
    public void setFilter(ArrayList<Customer> newList) {
        mCutomerList = new ArrayList<>();
        mCutomerList.addAll(newList);
        notifyDataSetChanged();
    }

    public class CustomerListViewHolder extends RecyclerView.ViewHolder {


        RelativeLayout mainLayoutMemberCustomer;
        RelativeLayout status_text_c;
        RelativeLayout status_text_s;
        TextView txt_name;
        TextView txt_number;
        View btn_edit;
        View btn_delete;


        public CustomerListViewHolder(View itemView) {
            super(itemView);

            mainLayoutMemberCustomer = itemView.findViewById(R.id.mainLayoutMemberCustomer);
            status_text_c = itemView.findViewById(R.id.status_text_c);
            status_text_s = itemView.findViewById(R.id.status_text_s);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_number = itemView.findViewById(R.id.txt_number);
            btn_edit = itemView.findViewById(R.id.btn_edit);
            btn_delete = itemView.findViewById(R.id.btn_delete);


        }


    }
    //endregion


}
