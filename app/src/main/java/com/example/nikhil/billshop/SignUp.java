package com.example.nikhil.billshop;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

import es.dmoral.toasty.Toasty;

public class SignUp extends AppCompatActivity {

    EditText et_Name, et_Mobile_Number, et_Email, et_Password, et_Businessname, et_ConfirmPassword, et_Address;
    Button btn_SignUp;
    TextView tv_Login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        AndroidNetworking.initialize(SignUp.this);

        hookUp();


        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String name = et_Name.getText().toString().toUpperCase();
                String mobileNumber = et_Mobile_Number.getText().toString();
                String businessName = et_Businessname.getText().toString();
                String emailId = et_Email.getText().toString();
                String address = et_Address.getText().toString();
                String password = et_Password.getText().toString();
                String confirmPassword = et_ConfirmPassword.getText().toString();


                if (et_Name.getText().toString().trim().equalsIgnoreCase("")) {
                    et_Name.setError("Name can not be blank");
                    return;
                }

                if (et_Mobile_Number.getText().toString().trim().equalsIgnoreCase("")) {
                    et_Mobile_Number.setError("Mobile can not be blank");
                    return;
                }

                if (et_Businessname.getText().toString().trim().equalsIgnoreCase("")) {
                    et_Businessname.setError("Business Name required");
                    return;
                }

                if (et_Address.getText().toString().trim().equalsIgnoreCase("")) {
                    et_Address.setError("Address required");
                    return;
                }

                if (et_Password.getText().toString().trim().equalsIgnoreCase("")) {
                    et_Password.setError("Password required");
                    return;
                }


                if (!password.equals(confirmPassword)) {
                    et_ConfirmPassword.setError("Password Not Matching");
                    return;
                }

                final String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

                Log.e("deviceId", "" + deviceId);


                try {


                    String url = "http://160.153.234.207:8080/billshop/webapi/user/registration?action=" + 1 +
                            "&" + "userid=" + 0 +
                            "&" + "username=" + name +
                            "&" + "mobileno=" + mobileNumber +
                            "&" + "email=" + emailId +
                            "&" + "address=" + address +
                            "&" + "businessname=" + businessName +
                            "&" + "deviceid=" + deviceId +
                            "&" + "ppassword=" + password +
                            "&" + "logedinuserid=" + 1;

                    Log.e("ChkUrl", url);


                    AndroidNetworking.post(url)
                            .setPriority(Priority.MEDIUM)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    int lastinsertid = response.optInt("result");
                                    String message = response.optString("message");
                                    int haserror = response.optInt("haserror");

                                    if (haserror == 0) {
                                        Toasty.success(SignUp.this, message, Toast.LENGTH_SHORT).show();

                                        onBackPressed();
                                    } else {
                                        Toasty.error(SignUp.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                    Log.e("AddCustomer", anError.getMessage());
                                }
                            });
                } catch (Exception ex) {
                    Log.e("AddCustomer", ex.getMessage());
                }


            }
        });


    }

    void hookUp() {
        et_Name = (EditText) findViewById(R.id.et_Name);
        et_Mobile_Number = (EditText) findViewById(R.id.et_Mobile_Number);
        et_Email = (EditText) findViewById(R.id.et_Email);
        et_Password = (EditText) findViewById(R.id.et_Password);
        et_Businessname = (EditText) findViewById(R.id.et_Businessname);
        et_ConfirmPassword = (EditText) findViewById(R.id.et_ConfirmPassword);
        et_Address = (EditText) findViewById(R.id.et_Address);
        btn_SignUp = (Button) findViewById(R.id.btn_SignUp);
    }


}
