package com.example.nikhil.billshop.Adapters.SpinnerAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.nikhil.billshop.Models.ProductCategories;
import com.example.nikhil.billshop.R;

import java.util.ArrayList;

public class SpinnerCategoryAdapter extends BaseAdapter {

    Context c;
    ArrayList<ProductCategories> categories;

    public SpinnerCategoryAdapter(Context context, ArrayList<ProductCategories> categories) {
        super();
        this.c = context;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ProductCategories cur_obj = categories.get(position);
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View row = inflater.inflate(R.layout.spinner_categorylist, parent, false);
        TextView tvCategoryName = (TextView) row.findViewById(R.id.tvCategoryName);
        tvCategoryName.setText(cur_obj.getCatname());

        return row;
    }
}