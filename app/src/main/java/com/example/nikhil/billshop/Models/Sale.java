package com.example.nikhil.billshop.Models;


public class Sale{


    int companyid;
    String createdat;
    int cstid;
    double finalamt;
    int gst;
    double gstamt;
    int pdtid;
    int saleid;
    int saleqty;
    double salerate;
    String companyname;
    int userid;

    public Sale() {
    }

    public Sale(int companyid, String createdat, int cstid, double finalamt, int gst, double gstamt, int pdtid, int saleid, int saleqty, double salerate, String companyname, int userid) {
        this.companyid = companyid;
        this.createdat = createdat;
        this.cstid = cstid;
        this.finalamt = finalamt;
        this.gst = gst;
        this.gstamt = gstamt;
        this.pdtid = pdtid;
        this.saleid = saleid;
        this.saleqty = saleqty;
        this.salerate = salerate;
        this.companyname = companyname;
        this.userid = userid;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public int getCstid() {
        return cstid;
    }

    public void setCstid(int cstid) {
        this.cstid = cstid;
    }

    public double getFinalamt() {
        return finalamt;
    }

    public void setFinalamt(double finalamt) {
        this.finalamt = finalamt;
    }

    public int getGst() {
        return gst;
    }

    public void setGst(int gst) {
        this.gst = gst;
    }

    public double getGstamt() {
        return gstamt;
    }

    public void setGstamt(double gstamt) {
        this.gstamt = gstamt;
    }

    public int getPdtid() {
        return pdtid;
    }

    public void setPdtid(int pdtid) {
        this.pdtid = pdtid;
    }

    public int getSaleid() {
        return saleid;
    }

    public void setSaleid(int saleid) {
        this.saleid = saleid;
    }

    public int getSaleqty() {
        return saleqty;
    }

    public void setSaleqty(int saleqty) {
        this.saleqty = saleqty;
    }

    public double getSalerate() {
        return salerate;
    }

    public void setSalerate(double salerate) {
        this.salerate = salerate;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "companyid=" + companyid +
                ", createdat='" + createdat + '\'' +
                ", cstid=" + cstid +
                ", finalamt=" + finalamt +
                ", gst=" + gst +
                ", gstamt=" + gstamt +
                ", pdtid=" + pdtid +
                ", saleid=" + saleid +
                ", saleqty=" + saleqty +
                ", salerate=" + salerate +
                ", companyname='" + companyname + '\'' +
                ", userid=" + userid +
                '}';
    }
}
