package com.example.nikhil.billshop.Models;

public class Counts {

    int count_customers;
    int count_products;
    int count_sales;
    int count_purchases;

    public Counts() {
        super();
    }

    public Counts(int count_customers, int count_products, int count_sales, int count_purchases) {
        super();
        this.count_customers = count_customers;
        this.count_products = count_products;
        this.count_sales = count_sales;
        this.count_purchases = count_purchases;
    }

    public int getCount_customers() {
        return count_customers;
    }

    public void setCount_customers(int count_customers) {
        this.count_customers = count_customers;
    }

    public int getCount_products() {
        return count_products;
    }

    public void setCount_products(int count_products) {
        this.count_products = count_products;
    }

    public int getCount_sales() {
        return count_sales;
    }

    public void setCount_sales(int count_sales) {
        this.count_sales = count_sales;
    }

    public int getCount_purchases() {
        return count_purchases;
    }

    public void setCount_purchases(int count_purchases) {
        this.count_purchases = count_purchases;
    }

    @Override
    public String toString() {
        return "Counts [count_customers=" + count_customers + ", count_products=" + count_products + ", count_sales="
                + count_sales + ", count_purchases=" + count_purchases + "]";
    }


}
