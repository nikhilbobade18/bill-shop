package com.example.nikhil.billshop.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Products implements Parcelable {


    int pdtid;
    int userid;
    int companyid;
    int categoryid;
    String categoryname;
    String pdtname;
    int pdtqty;
    int puomtype;
    double pdtrate;


    public Products() {
    }

    public Products(int pdtid, int userid, int companyid, int categoryid, String categoryname, String pdtname, int pdtqty, int puomtype, double pdtrate) {
        this.pdtid = pdtid;
        this.userid = userid;
        this.companyid = companyid;
        this.categoryid = categoryid;
        this.categoryname = categoryname;
        this.pdtname = pdtname;
        this.pdtqty = pdtqty;
        this.puomtype = puomtype;
        this.pdtrate = pdtrate;
    }

    protected Products(Parcel in) {
        pdtid = in.readInt();
        userid = in.readInt();
        companyid = in.readInt();
        categoryid = in.readInt();
        categoryname = in.readString();
        pdtname = in.readString();
        pdtqty = in.readInt();
        puomtype = in.readInt();
        pdtrate = in.readDouble();
    }

    public static final Creator<Products> CREATOR = new Creator<Products>() {
        @Override
        public Products createFromParcel(Parcel in) {
            return new Products(in);
        }

        @Override
        public Products[] newArray(int size) {
            return new Products[size];
        }
    };

    public int getPdtid() {
        return pdtid;
    }

    public void setPdtid(int pdtid) {
        this.pdtid = pdtid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getPdtname() {
        return pdtname;
    }

    public void setPdtname(String pdtname) {
        this.pdtname = pdtname;
    }

    public int getPdtqty() {
        return pdtqty;
    }

    public void setPdtqty(int pdtqty) {
        this.pdtqty = pdtqty;
    }

    public int getPuomtype() {
        return puomtype;
    }

    public void setPuomtype(int puomtype) {
        this.puomtype = puomtype;
    }

    public double getPdtrate() {
        return pdtrate;
    }

    public void setPdtrate(double pdtrate) {
        this.pdtrate = pdtrate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(pdtid);
        parcel.writeInt(userid);
        parcel.writeInt(companyid);
        parcel.writeInt(categoryid);
        parcel.writeString(categoryname);
        parcel.writeString(pdtname);
        parcel.writeInt(pdtqty);
        parcel.writeInt(puomtype);
        parcel.writeDouble(pdtrate);
    }
}
