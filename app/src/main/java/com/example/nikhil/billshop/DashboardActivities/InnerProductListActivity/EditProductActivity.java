package com.example.nikhil.billshop.DashboardActivities.InnerProductListActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.nikhil.billshop.DashboardActivities.ProductActivity;
import com.example.nikhil.billshop.Models.Products;
import com.example.nikhil.billshop.R;

import org.json.JSONObject;

import es.dmoral.toasty.Toasty;

public class EditProductActivity extends AppCompatActivity {

    Products products;

    EditText et_edit_prod_name, et_edit_qty, et_edit_UOM_IfOther, et_edit_rate;
    Spinner sp_edit_UOM, sp_edit_Category;

    View btn_edit_cat, btn_edit_uom;
    Button btn_edit_product;


    int uom;
    int catid;
    String catname;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        findViewById(R.id.btnBackEditProduct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        products = getIntent().getParcelableExtra("productDetails");
        Log.e("productDetails", products.getPdtname());

        hookUp();
        setToEditText();

        btn_edit_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProduct(products.getPdtid(), products.getUserid(), products.getCompanyid());
            }
        });

        //editCustomer();

        sp_edit_UOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                uom = position;
                Log.e("uom:", "" + uom);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_edit_Category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.e("uom:", "" + uom);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    void hookUp() {
        et_edit_prod_name = findViewById(R.id.et_edit_prod_name);
        et_edit_qty = findViewById(R.id.et_edit_qty);
        sp_edit_UOM = findViewById(R.id.sp_edit_UOM);
        sp_edit_Category = findViewById(R.id.sp_edit_Category);
        et_edit_rate = findViewById(R.id.et_edit_rate);

        btn_edit_product = findViewById(R.id.btn_edit_product);
    }

    void setToEditText() {
        et_edit_prod_name.setText(products.getPdtname());
        et_edit_qty.setText("" + products.getPdtqty());
        et_edit_rate.setText("" + products.getPdtrate());

    }


    void validateInputs() {
        if (et_edit_prod_name.getText().toString().trim().equalsIgnoreCase("")) {
            et_edit_prod_name.setError("Firstname can not be blank");
            return;
        }
        if (et_edit_qty.getText().toString().trim().equalsIgnoreCase("")) {
            et_edit_qty.setError("Last can not be blank");
            return;
        }
        if (et_edit_rate.getText().toString().trim().equalsIgnoreCase("")) {
            et_edit_rate.setError("Last can not be blank");
            return;
        }
        /*if (sp_edit_UOM.getS.toString().trim().equalsIgnoreCase("")) {
            sp_edit_UOM.setError("Mobile Number can not be blank");
            return;
        }
        if (sp_edit_Category.getText().toString().trim().equalsIgnoreCase("")) {
            sp_edit_Category.setError("Email can not be blank");
            return;
        }*/
    }

    private void editProduct(int pid, int userid, int compid) {


        try {

            validateInputs();


            String prodName = et_edit_prod_name.getText().toString().toUpperCase();
            int prodQty = Integer.parseInt(et_edit_qty.getText().toString());
            double prodRate = Double.parseDouble(et_edit_rate.getText().toString());


            String url = "http://160.153.234.207:8080/billshop/webapi/products/manage?action=" + 1 +
                    "&" + "pdtid=" + pid +
                    "&" + "userid=" + userid +
                    "&" + "companyid=" + compid +
                    "&" + "catid=" + 1 +
                    "&" + "catname=" + "Category" +
                    "&" + "pdtname=" + prodName +
                    "&" + "pdtqty=" + prodQty +
                    "&" + "puomtype=" + uom +
                    "&" + "pdtrate=" + prodRate +
                    "&" + "logedinuserid=" + userid;

            Log.e("ChkUrl", url);


            AndroidNetworking.get(url)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            int lastinsertid = response.optInt("result");
                            String message = response.optString("message");
                            int haserror = response.optInt("haserror");

                            if (haserror == 0) {
                                Toasty.success(EditProductActivity.this, message, Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(EditProductActivity.this, ProductActivity.class));
                                finish();

                                onBackPressed();
                            } else {
                                Toasty.success(EditProductActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ex) {
            Log.e("AddProduct", ex.getMessage());
        }
    }
}

