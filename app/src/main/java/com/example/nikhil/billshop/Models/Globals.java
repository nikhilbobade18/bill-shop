package com.example.nikhil.billshop.Models;

public class Globals {

    public static int userid;
    public static int companyid;


    public Globals() {
    }


    public Globals(int userid, int companyid) {
        this.userid = userid;
        this.companyid = companyid;
    }


    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }
}
